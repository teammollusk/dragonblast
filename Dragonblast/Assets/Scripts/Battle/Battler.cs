using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public abstract class Battler : GameFSMBehaviour
{
    private SpriteRenderer _defendIcon;
	private ShortStack<Action> _receivedActions = new ShortStack<Action>();
	private Transform _timelineIcon;
    private bool _isDefending = false;
    private Transform _rotationTracker;
    private float _barLength;
    private Vector3 _timelineStartPosition, _timelineEndPosition;
    private RectTransform _timelineTransform;
    private SpriteRenderer _timelineIconImage;
    private Dictionary<string, int> _statusEffects = new Dictionary<string, int>();
    private bool _killedSelf;
    private int _maxHealth;
    private Vector3 _startPosition;
    private Animator _animator;

    private const float DefendIconOffset = .8f;
    private const float PracticalTimelineWidthPercentage = (102f / 110f);
    private const float PracticalTimelineHeightPercentage = (10f / 14f);

    protected MovementController _movementController { get; private set; }
    protected Attacker _attacker { get; private set; }

    public GameObject TimelineIconPrefab { get; private set; }
    public string Name { get; private set; }
    public int Health { get; private set; }
    public int Damage { get; set; }
    public float Speed { get; set; }
    public float TimelinePosition { get; private set; }
    public GameObject Weapon { get; private set; }
    public Element[] Elements { get; private set; }
    public string[] Skills { get; private set; }
    public bool IsDefending { get { return _isDefending; } }
    public bool Dying { get; set; }
    public float AttackDistance { get; set; }
    public string StatusEffects
    {
        get
        {
            string[] effects = new string[_statusEffects.Keys.Count];
            _statusEffects.Keys.CopyTo(effects, 0);
            return string.Join(", ", effects);
        }
    }
    public RectTransform TimelineTransform
    {
        get
        {
            return _timelineTransform;
        }
        set
        {
            _timelineTransform = value;
            _timelineStartPosition = Camera.main.ScreenToWorldPoint(TimelineTransform.position + new Vector3(-1f * TimelineTransform.rect.width * PracticalTimelineWidthPercentage / 2f, TimelineTransform.rect.height * PracticalTimelineHeightPercentage / 2f, 10));
            _timelineEndPosition = Camera.main.ScreenToWorldPoint(TimelineTransform.position + new Vector3(TimelineTransform.rect.width * PracticalTimelineWidthPercentage / 2f, TimelineTransform.rect.height * PracticalTimelineHeightPercentage / 2f, 10));
            _barLength = (_timelineEndPosition - _timelineStartPosition).magnitude;
        }
    }
    public SpriteRenderer TimelineIconImage
    {
        get
        {
            return _timelineIconImage;
        }

        private set
        {
            // When setting the TimelineIconImage, adjust its start/end points on the timeline according to the size of the image (the bottom-right edge should touch the start and end positions)
            _timelineIconImage = value;
            var ppu = _timelineIconImage.sprite.pixelsPerUnit;
            var iconSpriteHeight = _timelineIconImage.sprite.rect.height / ppu;
            var iconSpriteWidth = _timelineIconImage.sprite.rect.width / ppu;
            var iconPositionOffset = new Vector3(-1 * iconSpriteWidth / 2, iconSpriteHeight / 2);
            _timelineStartPosition += iconPositionOffset;
            _timelineEndPosition += iconPositionOffset;
        }
    }

    public abstract void ChooseCombatAction();

    void Start()
    {
        // TODO: Instantiate timeline icon here
    }

    void OnDisable()
    {
        UnDefend();
        Destroy(this);
    }

    /// <summary>
    /// Called by child classes of Battler during their Create methods
    /// Keeps generic Battler param initialization in one place
    /// </summary>
    /// <param name="battler"></param>
    /// <param name="battlerGameObject"></param>
    /// <param name="partyMember"></param>
    /// <param name="destination"></param>
    /// <param name="facingDirection"></param>
    protected static void Init(Battler battler, GameObject battlerGameObject, PartyMember partyMember, Vector3 destination, string facingDirection, bool positionBonus)
    {
        battler.Name = partyMember.Name;
        battler.Health = partyMember.Health;
        battler.Damage = partyMember.Damage;
        battler.Speed = partyMember.Speed;
        battler.TimelinePosition = partyMember.StartingPosition + (positionBonus ? Constants.TimelinePositionBonus : 0);
        battler.TimelineIconPrefab = partyMember.TimelineIcon;
        battler.Weapon = partyMember.Weapon;
        battler.Skills = partyMember.Skills;

        var animator = battlerGameObject.GetComponent<Animator>();
        animator.runtimeAnimatorController = partyMember.AnimatorController;
        battler._animator = animator;

        battler._movementController = battlerGameObject.GetComponent<MovementController>();
        battler._movementController.Speed = Constants.BattlerMovementSpeed;
        battler._movementController.AutoMoveTo(destination, facingDirection);

        battler._attacker = battlerGameObject.GetComponent<Attacker>();

        battler._rotationTracker = battler.transform.Find(Constants.RotatorName).transform;

        battler.AttackDistance = partyMember.AttackDistance;
        battler._maxHealth = partyMember.Health;

        battler.GetComponent<SpriteRenderer>().color = Color.white;
    }

    public void EnableOnTimeline()
    {
        _timelineIcon = Instantiate(TimelineIconPrefab).transform;
        TimelineIconImage = _timelineIcon.GetComponent<SpriteRenderer>();
        _timelineIcon.transform.SetParent(TimelineTransform);
        _timelineIcon.transform.position = _timelineStartPosition;
        _timelineIcon.transform.position += new Vector3((TimelinePosition / 100) * _barLength, 0);
    }


    public bool AdvancePosition()
	{
		return ChangePosition(Speed/2);
	}

	public bool ChangePosition(float amount)
	{
        amount *= (Time.deltaTime / Constants.BaseDeltaTime);
		TimelinePosition += amount;
		
		if(TimelinePosition >= 100)
		{
			TranslatePointerToEnd();
			return true;
		}
		else
		{
            if(TimelinePosition < 0)
            {
                // If the position was moved backwards to before the start (<0), set the position to the start (0)
                var difference = 0 - TimelinePosition;
                amount += difference;
                TimelinePosition = 0;
            }
			TranslatePointerPosition(amount);
			return false;
		}
	}

	public bool RecieveAction(Action a)
	{
		_receivedActions.Push(a);
        return true; // a.Execute();
	}

	public void UndoLastAction()
	{
		_receivedActions.Pop().Undo();
	}

	public bool LoseHealth(int amount)
	{
        if(_isDefending)
        {
            // While defending take half as much damage (rounded down)
            amount = amount/2;
        }

        int r = 1, g = 0, b = 0;
        if(amount < 0)
        {
            r = 0;
            g = 1;
        }

        FloatingCombatText.Create(transform.position, Mathf.Abs(amount).ToString(), TimelineTransform, r, g, b);

        Health -= amount;
        if(Health > _maxHealth)
        {
            Health = _maxHealth;
        }
		else if(Health <= 0)
		{
            Die();
			return true;
		}

        return false;
    }

	public virtual bool LoseHealth(int amount, Element[] e)
	{
        return LoseHealth((int)(amount * Elements.Multiplier(e)));
	}

    public virtual void Defend()
    {
        _isDefending = true;
        var offsetVector = _rotationTracker.up * DefendIconOffset;
		_defendIcon = TheObjectPool.Get(PooledObjects.DefendIcon).GetComponent<SpriteRenderer>();
		_defendIcon.transform.position = transform.position + offsetVector;
		_defendIcon.gameObject.SetActive(true);
        _animator.Play(string.Format("{0}{1}", Constants.Defend, _movementController.FacingDirection));
    }

    public virtual void UnDefend()
    {
        _isDefending = false;
        if(_defendIcon != null)
        {
			_defendIcon.gameObject.SetActive(false);
			_defendIcon = null;
        }
    }

    public bool IsSelectingAction()
    {
        return TimelinePosition >= 100;
    }

    public void DestroyTimelineIcon()
    {
        Destroy(_timelineIcon.gameObject);
    }

    protected void TranslatePointerPosition(float translation)
    {
        if (_timelineIcon != null)
        {
            _timelineIcon.Translate(new Vector3(translation * _barLength / 100f, 0f, 0f));
        }
    }

    protected void ResetPointerPosition()
    {
        if (_timelineIcon != null)
        {
            _timelineIcon.position = _timelineStartPosition;
        }
    }

    private void TranslatePointerToEnd()
    {
        if (_timelineIcon != null)
        {
            _timelineIcon.position = _timelineEndPosition;
        }
    }

    public void FinishAction()
    {
        BattleManager.Instance.HideStatusPane();
        BattleManager.Instance.HideSkillPane();
        TickDownStatusEffects();
        ResetPointerPosition();
        TranslatePointerPosition(TimelinePosition -= 100);
        BattleManager.Instance.Resume();
    }

    private void Die()
    {
        Dying = true;
        BattleManager.Instance.RemoveBattler(this);
        StartCoroutine(FadeOut());
    }

    private IEnumerator FadeOut()
    {
        var sprite = GetComponent<SpriteRenderer>();
        var r = sprite.color.r;
        var g = sprite.color.g;
        var b = sprite.color.b;
        yield return new WaitForSeconds(.2f);
        while(sprite.color.a > 0)
        {
            sprite.color = new Color(r, g, b, sprite.color.a - .03f);
            yield return new WaitForSeconds(.01f);
        }
        Dying = false;
        Destroy();
    }

    public void Destroy()
    {
        // FinishAction is usually called after the target dies, but if the target is yourself, you have to call it before destroying yourself.
        if(_killedSelf)
        {
            FinishAction();
        }
        gameObject.SetActive(false);
    }

    public IEnumerator MoveAndAttack(Battler target, AttackAction action, Element[] damageType)
    {
        _startPosition = transform.position;

        // Hide defend icon if it exists
        if (_defendIcon != null)
            _defendIcon.enabled = false;

        if (target != this) // If you're hitting yourself you don't have to move
        {
            // Move to just before the enemy
            yield return MoveNextTo(target);
        }

        // Animate attack
        yield return AnimateAttack();

        // Do damage
        target.RecieveAction(action);
        target.LoseHealth(this.Damage, damageType);

        // Move back to original position
        yield return MoveBack();

        // Show defend icon if it exists
        if (_defendIcon != null)
            _defendIcon.enabled = true;

        // Wait for dying animation to finish before resuming battle
        while (target.Dying)
        {
            if(target == this)
            {
                _killedSelf = true;
            }
            yield return new WaitForEndOfFrame();
        }

        // Resume battle
        FinishAction();
    }

    public IEnumerator AnimateAttack()
    {
        var animationClipName = Constants.Attack + _movementController.FacingDirection;
        _attacker.Attack(this.Weapon);
        yield return new WaitForSeconds(.1f + Utils.GetAnimationClipLength(_animator, animationClipName));
    }

    public void AnimateAttackNoWait()
    {
        _attacker.Attack(this.Weapon);
    }

    public IEnumerator MoveTo(Vector3 position)
    {
        _startPosition = transform.position;
        _movementController.AutoMoveTo(position);

        while (_movementController.IsAutoMoving)
        {
            yield return new WaitForEndOfFrame();
        }
    }

    public IEnumerator MoveNextTo(Battler target)
    {
        var attackPosition = GetAttackPosition(target);
        yield return MoveTo(attackPosition);
    }

    public IEnumerator MoveBack()
    {
        _movementController.AutoMoveTo(_startPosition);

        yield return new WaitUntil(() => !_movementController.IsAutoMoving);
    }

    public virtual Vector3 GetAttackPosition(Battler target)
    {
        var positionOffset = Utils.GetVectorFromDirectionString(_movementController.FacingDirection) * -1 * AttackDistance;
        var position = target.transform.position + positionOffset;
        return position;
    }

    public void AddStatusEffect(string name, int duration)
    {
        if(_statusEffects.ContainsKey(name))
        {
            _statusEffects[name] = duration;
        }
        else
        {
            _statusEffects.Add(name, duration);
        }
    }

    public bool IsStatusEffectActive(string name)
    {
        return _statusEffects.ContainsKey(name);
    }

    private void TickDownStatusEffects()
    {
        var effects = new List<string>(_statusEffects.Keys);
        foreach(string effect in effects)
        {
            int newDuration = _statusEffects[effect] - 1;
            if(newDuration == 0)
            {
                _statusEffects.Remove(effect);
                if(Constants.SkillsDictionary.ContainsKey(effect))
                {
                    var skill = Constants.SkillsDictionary[effect];
                    skill.User = this;
                    skill.Undo();

                }
            }
            else
            {
                _statusEffects[effect] = newDuration;
            }
        }
    }
}
