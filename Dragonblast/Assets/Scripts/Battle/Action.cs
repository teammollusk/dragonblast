﻿/* Base class for attacks and abilities.
 * Usage: these objects will be created, given a target, and
 * 		  sent to the target to be stored. The target, upon 
 * 		  receiving the object will call the execute method.
 * 		  The execute method is where the logic for the action
 * 		  is to be put.
 * 		  If another ability would undo an action then it
 * 		  will do so through the undo method.
 * @Author: Christian Casadio
 */
public abstract class Action
{
    public Battler User { get; set; }
    public Battler Target { get; set; }

	/* stores a copy of any variables that are altered and performs action specified by implementing class
	 * @return returns if the battler is alive
	 */
	public abstract void Execute();

	/* uses variables stored by execute to revert to last state
	 * @return returns if the battler is alive
	 */
	public abstract void Undo();
}