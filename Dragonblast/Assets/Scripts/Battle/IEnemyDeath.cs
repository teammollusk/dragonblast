﻿using UnityEngine;
using System.Collections;

public interface IEnemyDeath
{
    void Execute();
}
