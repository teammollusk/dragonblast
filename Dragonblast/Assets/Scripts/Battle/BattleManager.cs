﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class BattleManager : GameFSMBehaviour
{
    public static BattleManager Instance;

    public AllyBattler[] Allies { get; private set; }
    public EnemyBattler[] Enemies { get; private set; }
    public Vector3 BattleCenter { get; private set; }

    private const float TimelineUpdateDelay = 0.01f;

    private bool _isTimelineAwake = false;
    private bool _isActionBeingChosen = false;
	private List<Battler> _currentBattlers;
    private Timeline _timeline;
    private ActionSelectionPane _actionSelectionPane;
    private SkillsSelectionPane _skillSelectionPane;
    private TargetSelector _targetSelector;
    private StatusPane _statusPane;
    private int _allyCount;
    private int _enemyCount;
    private Player _overworldPlayer;
    private Enemy _overworldEnemy;
    private List<Battler> _sortedBattlers;

    void Awake()
    {
		if (Instance)
			Debug.LogError("Attempted to create second instance of a singleton");

		Instance = this;
	}

    public void StartBattle(Timeline timeline, AllyBattler[] allies, EnemyBattler[] enemies, GameObject playerGameObject, Enemy overworldEnemy, Vector3 battleCenter)
    {
        _isTimelineAwake = true;
        _isActionBeingChosen = false;
        _currentBattlers = null;

        _timeline = timeline;
        Allies = allies;
        Enemies = enemies;
        _allyCount = 0;
        _enemyCount = 0;
        _actionSelectionPane = timeline.GetComponentInChildren<ActionSelectionPane>();
        _skillSelectionPane = timeline.GetComponentInChildren<SkillsSelectionPane>();
        _statusPane = timeline.GetComponentInChildren<StatusPane>();
        _targetSelector = timeline.GetComponentInChildren<TargetSelector>();
        _targetSelector.Init(allies, enemies);
        _overworldPlayer = playerGameObject.GetComponent<Player>();
        _overworldEnemy = overworldEnemy;
        _sortedBattlers = new List<Battler>();
        BattleCenter = battleCenter;

        foreach(AllyBattler ab in allies)
        {
            if (ab)
            {
                _sortedBattlers.Add(ab);
                _allyCount++;
            }
        }
        foreach (EnemyBattler eb in enemies)
        {
            if (eb)
            {
                _sortedBattlers.Add(eb);
                _enemyCount++;
            }
        }
    }
	
	public override void UpdateCombat() 
	{
		if(_isTimelineAwake && !_isActionBeingChosen)
		{
			if(_currentBattlers != null && !(_currentBattlers.Count == 0))
			{
				_isActionBeingChosen = true;
                SortBattlersAndLayerTimelineIcons(); // If icons are overlapping, show the ones further along the timeline on top
                _currentBattlers[0].ChooseCombatAction();
                _currentBattlers.RemoveAt(0);
			}
            else
            {
                _currentBattlers = _timeline.Advance();
                StartCoroutine(SleepTimeline(TimelineUpdateDelay));
            }
        }
	}

    private void SortBattlersAndLayerTimelineIcons()
    {
        _sortedBattlers.Sort(CompareBattlersByTimelinePosition);
        LayerTimelineIcons();
    }

    public void LayerTimelineIcons()
    {
        _sortedBattlers.Sort(CompareBattlersByTimelinePosition);
        var thisLayer = Constants.TimelineIconLayer;
        for(int i = 0; i < _sortedBattlers.Count; i++)
        {
            Battler thisBattler = _sortedBattlers[i];
            if (thisBattler != null && thisBattler.gameObject.activeInHierarchy && !_currentBattlers.Contains(thisBattler))
                thisBattler.TimelineIconImage.sortingOrder = thisLayer++;
        }
        for(int i = _currentBattlers.Count - 1; i >= 0; i--)
        {
            _currentBattlers[i].TimelineIconImage.sortingOrder = thisLayer++;
        }
    }

    private int CompareBattlersByTimelinePosition(Battler x, Battler y)
    {
        return x.TimelinePosition.CompareTo(y.TimelinePosition);
    }

    public void Resume()
    {
        _isActionBeingChosen = false;

        if (_enemyCount == 0)
        {
            Win();
        }
        else if (_allyCount == 0)
        {
            Lose();
        }
    }

    #region ActionSelectionPane methods
    public void ShowActionPane()
    {
        _actionSelectionPane.Show();
    }

    public void HideActionPane()
    {
        _actionSelectionPane.Hide();
    }

    public string GetSelectedAction()
    {
        return _actionSelectionPane.GetSelectedAction();
    }

    public bool IsActionPaneOpen()
    {
        return _actionSelectionPane.IsOpen();
    }
    #endregion

    #region SkillSelectionPane methods
    public void ShowSkillPane(string[] skillList)
    {
        _skillSelectionPane.Show();
        _skillSelectionPane.SetActions(skillList);
    }

    public void HideSkillPane()
    {
        _skillSelectionPane.Hide();
    }

    public string GetSelectedSkill()
    {
        return _skillSelectionPane.GetSelectedAction();
    }

    public bool IsSkillPaneOpen()
    {
        return _skillSelectionPane.IsOpen();
    }
    #endregion

    #region TargetSelector methods
    public void ShowTargetSelector()
    {
        _targetSelector.Show();
    }

    public void HideTargetSelector()
    {
        _targetSelector.Hide();
    }

    public bool IsTargetSelectorShown()
    {
        return _targetSelector.IsShown();
    }

    public Battler GetTarget()
    {
        return _targetSelector.TargetedBattler;
    }

    public void RemoveBattler(Battler battler)
    {
        _currentBattlers.Remove(battler);
        _timeline.RemoveBattler(battler);
        _targetSelector.RemoveBattler(battler);

        if (battler is AllyBattler)
        {
            _allyCount--;
        }
        else if(battler is EnemyBattler)
        {
            _enemyCount--;
        }
    }
    #endregion

    #region StatusPane methods
    public void ShowStatusPane()
    {
        _statusPane.Show();
    }

    public void HideStatusPane()
    {
        _statusPane.Hide();
    }

    public void UpdateStatusPane(Battler battler)
    {
        _statusPane.UpdatePane(battler);
    }

    public void UpdateStatusPane(string text)
    {
        _statusPane.UpdatePane(text);
    }
    #endregion

    public void Win()
    {
        StartCoroutine(EndBattle());
        _overworldEnemy.Die();
    }

    public void Lose()
    {
        StartCoroutine(LoseBattle());
    }

    public void Run()
    {
        StartCoroutine(EndBattle());
    }

    private IEnumerator LoseBattle()
    {
        _isTimelineAwake = false;
        GameOver.Instance.Execute();
        yield return new WaitForSeconds(4f);
        _timeline.Destruct();
        DestroyTimelineIcons();
        _overworldEnemy.Reset();
    }

    private IEnumerator EndBattle()
    {
        _isTimelineAwake = false;

        // IF the party leader died, set the next battler as the new leader
        var partyLeaderBattler = Allies[0];
        if(Allies[0] == null || !Allies[0].gameObject.activeInHierarchy)
        {
            for(int i = 1; i < Allies.Length; i++)
            {
                if(Allies[i] != null && Allies[i].gameObject.activeInHierarchy)
                {
                    partyLeaderBattler = Allies[i];
                    break;
                }
            }
        }
        //var partyLeader = TheParty.Instance.GetPartyMember(partyLeaderBattler.Name);
        //TheParty.Instance.SetPartyLeader(partyLeader); Commenting for now until we have better working leader switching

        // Make all allies walk into the party leader
        var allyToPosition = partyLeaderBattler.transform.position;
        for(int i = 0; i < Allies.Length; i++)
        {
            if(Allies[i] != null && Allies[i] != partyLeaderBattler)
            {
                Allies[i].UnDefend();
                Allies[i].GetComponent<MovementController>().AutoMoveTo(allyToPosition);
            }
        }

        // Make all enemies walk to where the orignal enemy was
        if(_enemyCount != 0) // Player ran away
        {
            var enemyToPosition = _overworldEnemy.transform.position;
            for(int i = 0; i < Enemies.Length; i++)
            {
                if (Enemies[i] != null && Enemies[i].gameObject.activeInHierarchy)
                {
                    Enemies[i].GetComponent<MovementController>().AutoMoveTo(enemyToPosition);
                }
            }
        }

		// Hide timeline
		_timeline.Hide();
        DestroyTimelineIcons();

        // Pan camera/player to party leader and reshow overworld characters
        yield return StartCoroutine(_overworldPlayer.PanToPosition(allyToPosition));
        _overworldPlayer.Show();
        if (_enemyCount != 0 && _allyCount != 0) // Player ran away
        {
            _overworldEnemy.StartBlinking();
        }

		// Self destruct
		TheGame.TriggerState(GameState.Overworld);
        _timeline.Destruct();
    }

    private void DestroyTimelineIcons()
    {
        foreach (AllyBattler ab in Allies)
        {
            if (ab != null && ab.TimelineIconImage != null)
            {
                Destroy(ab.TimelineIconImage.gameObject);
            }
        }
        foreach (EnemyBattler eb in Enemies)
        {
            if (eb != null && eb.TimelineIconImage != null)
            {
                Destroy(eb.TimelineIconImage.gameObject);
            }
        }
    }

    private IEnumerator SleepTimeline(float time)
	{
		_isTimelineAwake = false;
		yield return new WaitForSeconds(time);
		_isTimelineAwake = true;
	}
}