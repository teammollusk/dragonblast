﻿//enum for damage types
public enum Element {Wind, Fire, Water, Corn};

/* class to contain a method that can be called on an element
 * to determine if one element is weak or strong against another. 
 * Ex1: Element.Fire.multiplier(Element.Water);
 * In the example above Fire is being attacked by water so the
 * multiplier will be larger than one since fire is weak to water.
 * Ex2: Element.Water.multiplier(Element.Fire);
 * In this case the roles are reversed and the multiplier returned
 * will be less than one since Water is strong against Fire.
 * Ex3: Element.Water.multiplier(Element.Wind);
 * In this case the elements are neither strong nor weak against
 * each other so the multiplier will be one.
 */
public static class ElementAffinity
{
	public static double Multiplier(this Element[] attackingElements, Element[] defendingElements)
	{
		//logic to determine the damage multiplier
		double mult = 1;
        if(attackingElements == null || defendingElements == null)
        {
            return mult;
        }
		foreach(Element ae in attackingElements)
		{
			foreach(Element de in defendingElements)
			{
				if((int)ae + 1 == (int)de)
				{
					mult *= 2;
				}
				else if((int)de + 1 == (int)ae)
				{
					mult /= 2;
				}
			}
		}
		
		return mult;
	}

	public static double Multiplier(this Element[] attackingElements, Element defendingElement)
	{
		//logic to determine the damage multiplier
		double mult = 1;
		foreach(Element ae in attackingElements)
		{
			if((int)ae + 1 == (int)defendingElement)
			{
				mult *= 2;
			}
			else if((int)defendingElement + 1 == (int)ae)
			{
				mult /= 2;
			}
		}
		
		return mult;
	}

	public static double Multiplier(this Element attackingElement, Element[] defendingElements)
	{
		//logic to determine the damage multiplier
		double mult = 1;

		foreach(Element de in defendingElements)
		{
			if((int)attackingElement + 1 == (int)de)
			{
				mult *= 2;
			}
			else if((int)de + 1 == (int)attackingElement)
			{
				mult /= 2;
			}
		}

		return mult;
	}

	public static double Multiplier(this Element attackingElement, Element defendingElement)
	{
		//logic to determine the damage multiplier
		double mult = 1;

		if((int)attackingElement + 1 == (int)defendingElement)
		{
			mult *= 2;
		}
		else if((int)defendingElement + 1 == (int)attackingElement)
		{
			mult /= 2;
		}
		
		return mult;
	}
}