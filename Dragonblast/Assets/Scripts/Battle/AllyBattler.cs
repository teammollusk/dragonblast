﻿using UnityEngine;
using System.Collections;
using System;

public class AllyBattler : Battler
{
    Action _selectedAction;

    /// <summary>
    /// Get a generic battler prefab from the object pool, add an AllyBattler script to it, and initialize its params
    /// </summary>
    /// <param name="battlerPrefab"></param>
    /// <param name="partyMember"></param>
    /// <param name="startPosition"></param>
    /// <param name="destination"></param>
    /// <param name="facingDirection"></param>
    /// <returns></returns>
    public static AllyBattler Create(PartyMember partyMember, Vector3 startPosition, Vector3 destination, string facingDirection, bool playerWeaponHit)
    {
        GameObject battlerGameObject = TheObjectPool.Get(PooledObjects.Battler);
        battlerGameObject.transform.SetPositionAndRotation(startPosition, Quaternion.identity);
        AllyBattler ab = battlerGameObject.AddComponent<AllyBattler>();
        battlerGameObject.tag = Constants.AllyBattlerTag;

        Init(ab, battlerGameObject, partyMember, destination, facingDirection, playerWeaponHit);

        // Initialize Ally specific params here
        var spriteRenderer = battlerGameObject.GetComponent<SpriteRenderer>();
        spriteRenderer.sortingLayerName = Constants.PlayerLayer;

        battlerGameObject.SetActive(true);
        return ab;
    }

    public override void ChooseCombatAction()
    {
        BattleManager.Instance.ShowActionPane();
        BattleManager.Instance.ShowStatusPane();
        BattleManager.Instance.UpdateStatusPane(this);
        UnDefend();
    }

    public override void UpdateCombat()
    {
        if(IsSelectingAction())
        {
            // Select target
            if(BattleManager.Instance.IsTargetSelectorShown())
            {
                if(Input.GetButtonDown(Constants.SelectInput))
                {
                    SelectTarget();
                }
                else if(Input.GetButtonDown(Constants.CancelInput))
                {
                    CancelAction();
                }
            }
            // Select action
            else if(BattleManager.Instance.IsActionPaneOpen())
            {
                if(Input.GetButtonDown(Constants.SelectInput))
                {
                    SelectAction();
                }
            }
            else if(BattleManager.Instance.IsSkillPaneOpen())
            {
                DisplaySkillDescription();
                if (Input.GetButtonDown(Constants.SelectInput))
                {
                    SelectSkill();
                }
                else if (Input.GetButtonDown(Constants.CancelInput))
                {
                    CancelAction();
                }
            }
        }
    }

    private void DisplaySkillDescription()
    {
        var skillString = BattleManager.Instance.GetSelectedSkill();
        if (Constants.SkillsDictionary.ContainsKey(skillString))
        {
            Skill skill = Constants.SkillsDictionary[skillString];
            BattleManager.Instance.UpdateStatusPane(skill.Description);
        }
        else
        {
            Debug.LogError(string.Format("An undefined skill ({0}) was hovered over by {1}", skillString, Name));
            FinishAction();
        }
    }

    private void SelectSkill()
    {
        BattleManager.Instance.HideSkillPane();
        BattleManager.Instance.HideStatusPane();

        var skillString = BattleManager.Instance.GetSelectedSkill();
        if (Constants.SkillsDictionary.ContainsKey(skillString))
        {
            Skill skill = Constants.SkillsDictionary[skillString];
            skill.User = this;
            skill.Execute();
        }
        else
        {
            Debug.LogError(string.Format("An undefined skill ({0}) was used by {1}", skillString, Name));
            FinishAction();
        }
    }

    private void SelectAction()
    {
        BattleManager.Instance.HideActionPane();
        BattleManager.Instance.HideStatusPane();

        var actionName = BattleManager.Instance.GetSelectedAction();

        switch(actionName)
        {
            case Constants.AttackAction:
                Attack();
                break;
            case Constants.SkillsAction:
                UseSkills();
                break;
            case Constants.DefendAction:
                Defend();
                FinishAction();
                break;
            default:
                Debug.LogError(string.Format("Unsupported action was selected: {0}", actionName));
                break;
        }
    }

    private void Attack()
    {
        _selectedAction = new AttackAction(this);
        BattleManager.Instance.ShowTargetSelector();
    }

    private void UseSkills()
    {
        BattleManager.Instance.ShowSkillPane(Skills);
        BattleManager.Instance.ShowStatusPane();
    }

    private void CancelAction()
    {
        BattleManager.Instance.ShowActionPane();
        BattleManager.Instance.UpdateStatusPane(this); 
        BattleManager.Instance.HideTargetSelector();
        BattleManager.Instance.HideSkillPane();
    }

    private void SelectTarget()
    {
        _selectedAction.Target = BattleManager.Instance.GetTarget();
        BattleManager.Instance.HideTargetSelector();
        _selectedAction.Execute();
    }
}