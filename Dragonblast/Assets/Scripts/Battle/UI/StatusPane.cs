﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StatusPane : MonoBehaviour
{

    [SerializeField]
    private Image _background = default;
    [SerializeField]
    private Text _details = default;

    private bool _isOpen = false;

    public void Hide()
    {
        _background.enabled = false;
        _details.enabled = false;
        _isOpen = false;
    }

    public void Show()
    {
        _background.enabled = true;
        _details.enabled = true;
        _isOpen = true;
    }

    public void UpdatePane(Battler battler)
    {
        _details.text = string.Format("{0}\n{1} HP\n{2}", battler.Name, battler.Health, battler.StatusEffects);
        _background.enabled = true;
        _details.enabled = true;
    }

    public void UpdatePane(string text)
    {
        _details.text = text;
    }

    public bool IsOpen()
    {
        return _isOpen;
    }
}
