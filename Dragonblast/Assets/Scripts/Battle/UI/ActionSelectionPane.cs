﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ActionSelectionPane : MonoBehaviour {

    [SerializeField]
    private Image _background = default;
    [SerializeField]
    private Image _selector = default;
    [SerializeField]
    private Text _actions = default;
    [SerializeField]
    private RectTransform _selectorRect = default;
    
    private bool _selectorAwake = true;
    private float _selectorMaxPosition;
    private Vector3 _selectorDistanceUnit;
    private float _selectorHeight;
    private short _actionIndex = 0;
    private bool _isOpen = false;
    private string[] _actionsList = Constants.BattleActions;

    private const float SelectorSleepTime = .3f;
    private const float SelectorOffsetMultipler = 1.7f;

    public void Start()
    {
        _selectorMaxPosition = _selectorRect.localPosition.y;
        _selectorHeight = _selector.rectTransform.rect.height;
        _selectorDistanceUnit = new Vector2(0, _selectorHeight * SelectorOffsetMultipler);
        SetActions(_actionsList);
    }

    public void Update()
    {
        var verticalInput = Input.GetAxisRaw("Vertical");
        if (verticalInput == 0)
        {
            _selectorAwake = true;
            return;
        }

        if (_selectorAwake)
        {
            if (verticalInput > 0 && _actionIndex > 0)
            {
                _selectorRect.position += _selectorDistanceUnit;
                _actionIndex--;
            }
            else if (verticalInput < 0 && _actionIndex < _actionsList.Length - 1)
            {
                _selectorRect.position -= _selectorDistanceUnit;
                _actionIndex++;
            }
            StopCoroutine("SleepSelector");
            StartCoroutine("SleepSelector");
        }
    }

	public void SetActions(string[] actionsList)
	{
		_actionsList = actionsList;
		_actions.text = string.Join("\n", _actionsList);
	}

	public void Hide()
    {
        _background.enabled = false;
        _selector.enabled = false;
        _actions.enabled = false;
        _isOpen = false;
    }

    public void Show()
    {
        _selectorRect.localPosition = new Vector2(_selectorRect.localPosition.x, _selectorMaxPosition);
        _actionIndex = 0;

        _background.enabled = true;
        _selector.enabled = true;
        _actions.enabled = true;
        _isOpen = true;
    }

    public bool IsOpen()
    {
        return _isOpen;
    }

    public string GetSelectedAction()
    {
        return _actionsList[_actionIndex];
    }

    private IEnumerator SleepSelector()
    {
        _selectorAwake = false;
        yield return new WaitForSeconds(SelectorSleepTime);
        _selectorAwake = true;
    }
}
