﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timeline : MonoBehaviour
{
    private Battler[] _battlers;
	private Canvas _canvas;
    public RectTransform CanvasTransform { get; set; }

	private void Awake()
	{
		_canvas = GetComponent<Canvas>();
	}

	private void OnEnable()
	{
		Show();
	}

	public void Hide()
	{
		_canvas.enabled = false;
	}

	public void Show()
	{
		_canvas.enabled = true;
	}
	
	public static Timeline Create(Battler[] battlers)
	{
		GameObject timelineObject = TheObjectPool.Get(PooledObjects.Timeline);
        Timeline timeline = timelineObject.GetComponent<Timeline>();
        timeline._battlers = new Battler[battlers.Length];
        var timelineImage = timelineObject.transform.Find("TimelineImage") as RectTransform;
        timeline.CanvasTransform = timelineImage;
        for (int i = 0; i < battlers.Length; i++) 
		{
            if(battlers[i])
            {
                timeline._battlers[i] = battlers[i];
                timeline._battlers[i].TimelineTransform = timelineImage;
                timeline._battlers[i].EnableOnTimeline();
            }
        }

		timelineObject.SetActive(true);

        return timeline;
	}

    //returns a battler that has reached the end of the timeline, returns null otherwise
    public List<Battler> Advance()
	{
        List<Battler> readyBattlers = new List<Battler>();

        foreach(Battler battler in _battlers)
        {
            if (battler == null || !battler.gameObject.activeInHierarchy) continue;
            bool ready = battler.AdvancePosition();
            if(ready)
            {
                readyBattlers.Add(battler);
            }
        }

        if(readyBattlers.Count != 0)
        {
            return readyBattlers;
        }

		return null;
	}

    public void RemoveBattler(Battler removedBattler)
    {
        for(int i = 0; i < _battlers.Length; i++)
        {
            if(_battlers[i] == removedBattler)
            {
                _battlers[i].DestroyTimelineIcon();
                _battlers[i] = null;
            }
        }
    }

    public void Destruct()
    {
        foreach(Battler battler in _battlers)
        {
            if(battler != null)
            {
                battler.gameObject.SetActive(false);
            }
        }
		gameObject.SetActive(false);
    }
}
