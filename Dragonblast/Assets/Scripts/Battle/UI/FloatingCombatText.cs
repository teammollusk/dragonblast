﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FloatingCombatText : MonoBehaviour
{
    private const float AlphaStep = .05f;
    private const float DelayBetweenSteps = .05f;
    private const float InitialDelay = .25f;
    private readonly float PositionStep = .5f * Screen.width / 975;

    private RectTransform _rectTransform;
    private Text _textComponent;
    private string _text;
    private Vector3 _position;
    private Color _color;
	private Transform _originalParent;

    public static void Create(Vector3 position, string text, Transform timelineTransform, float r = 1, float g = 1, float b = 1)
    {
		GameObject fctObject = TheObjectPool.Get(PooledObjects.FloatingCombatText);

        var fct = fctObject.GetComponent<FloatingCombatText>();
        fct._text = text;
        fct._position = position;
        fct._color = new Color(r, g, b);

		fct._originalParent = fct.transform.parent;
		fctObject.transform.SetParent(timelineTransform, true); // So it'll disappear when battle ends instead of weirdly panning over with the camera
		fctObject.SetActive(true);

	}

	private void OnEnable()
	{
        _rectTransform = GetComponent<RectTransform>();
		_rectTransform.position = Camera.main.WorldToScreenPoint(_position + new Vector3(0.5f, 0.5f));
        _textComponent = GetComponent<Text>();
        _textComponent.text = _text;
        _textComponent.color = _color;
        StartCoroutine(FadeOut());
	}

    private IEnumerator FadeOut()
    {
        // Start the slight movement of the text before the fade out to make it easier to read
        float initialDelayCounter = 0f;
        while(initialDelayCounter < InitialDelay)
        {
            _rectTransform.Translate(PositionStep, PositionStep, 0);
            initialDelayCounter += DelayBetweenSteps;
            yield return new WaitForSeconds(DelayBetweenSteps);
        }

        // Now start fading out while continuing to pan text
        while(_textComponent.color.a > 0)
        {
            _textComponent.color = new Color(_textComponent.color.r, _textComponent.color.g, _textComponent.color.b, _textComponent.color.a - AlphaStep);
            _rectTransform.Translate(PositionStep, PositionStep, 0);
            yield return new WaitForSeconds(DelayBetweenSteps);
        }

		gameObject.SetActive(false);
		transform.SetParent(_originalParent);
    }

}
