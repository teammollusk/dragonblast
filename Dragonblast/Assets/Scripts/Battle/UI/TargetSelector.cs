﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class TargetSelector : MonoBehaviour
{
    public Battler TargetedBattler { get; set; }

    [SerializeField]
    private SpriteRenderer _sprite = default;

    private const float ShowTime = .5f;
    private const float HideTime = .2f;
    private bool _isShown = false;
    private bool _selectorAwake = true;
    private List<Battler> _battlers;

    private readonly Vector3 PositionOffset = new Vector3(0, .8f);
    private const float SelectorSleepTime = .3f;

    public void Update()
    {
        if(!_isShown)
        {
            return;
        }

        var verticalInput = Input.GetAxisRaw("Vertical");
        var horizontalInput = Input.GetAxisRaw("Horizontal");

        if(Mathf.Abs(verticalInput) <= .5f && Mathf.Abs(horizontalInput) <= 0.5f)
        {
            _selectorAwake = true;
            return;
        }

        if (_selectorAwake)
        {
            Battler nextBattler = null;
            if (Math.Abs(verticalInput) > Math.Abs(horizontalInput))
            {
                if (verticalInput > 0.5f)
                {
                    nextBattler = MoveUp();
                }
                else if (verticalInput < -0.5f)
                {
                    nextBattler = MoveDown();
                }
            }
            else
            {
                if (horizontalInput > 0.5f)
                {
                    nextBattler = MoveRight();
                }
                else if (horizontalInput < -0.5f)
                {
                    nextBattler = MoveLeft();
                }
            }

            if(nextBattler != null && nextBattler != TargetedBattler) 
            {
                StopCoroutine("Blink");
                StopIconFlash();
                TargetedBattler = nextBattler;
                StartCoroutine("Blink");
                StartIconFlash();

                UpdatePosition(TargetedBattler.transform.position);
                BattleManager.Instance.UpdateStatusPane(TargetedBattler);

                StopCoroutine("SleepSelector");
                StartCoroutine("SleepSelector");
            }
        }
        
    }

	public void Init(AllyBattler[] allies, EnemyBattler[] enemies)
	{
		_battlers = new List<Battler>(allies.Length + enemies.Length);
		for (int i = 0; i < enemies.Length; i++)
		{
			if (enemies[i])
				_battlers.Add(enemies[i]);
		}
		for (int i = 0; i < allies.Length; i++)
		{
			if (allies[i])
				_battlers.Add(allies[i]);
		}

		TargetedBattler = _battlers[0];
	}

	public void Show()
	{
		UpdatePosition(TargetedBattler.transform.position);
		StartCoroutine("Blink");
		StartIconFlash();
		_isShown = true;
		BattleManager.Instance.UpdateStatusPane(TargetedBattler);
	}

	public void Hide()
	{
		StopCoroutine("Blink");
		StopIconFlash();
		_sprite.enabled = false;
		_isShown = false;
		if (_battlers.Count > 0)
		{
			TargetedBattler = _battlers[0];
		}
		else
		{
			TargetedBattler = null;
		}

	}

	public bool IsShown()
	{
		return _isShown;
	}

	public void RemoveBattler(Battler battler)
    {
        _battlers.Remove(battler);
        if(_battlers.Count != 0)
        {
            TargetedBattler = _battlers[0];
        }
        else
        {
            TargetedBattler = null;
        }
    }

    private void UpdatePosition(Vector3 newPosition)
    {
        transform.position = newPosition + PositionOffset;
    }

    private IEnumerator Blink()
    {
        while(true)
        {
            _sprite.enabled = true;
            yield return new WaitForSeconds(ShowTime);
            _sprite.enabled = false;
            yield return new WaitForSeconds(HideTime);
        }
    }

    private void StopIconFlash()
    {
        StopCoroutine("FlashTargetedIcon");
        TargetedBattler.TimelineIconImage.color = Color.white;
    }

    private void StartIconFlash()
    {
        StartCoroutine("FlashTargetedIcon");
    }

    private IEnumerator FlashTargetedIcon()
    {
        // If the targetted icon is overlapping others, display it on top
        BattleManager.Instance.LayerTimelineIcons();
        TargetedBattler.TimelineIconImage.sortingOrder += _battlers.Count;

        // Flash timeline icon red to show it corresponds to the enemy targeted
        while (true)
        {
            TargetedBattler.TimelineIconImage.color = Color.red;
            yield return new WaitForSeconds(ShowTime);
            TargetedBattler.TimelineIconImage.color = Color.white;
            yield return new WaitForSeconds(HideTime);
        }
    }

    private IEnumerator SleepSelector()
    {
        _selectorAwake = false;
        yield return new WaitForSeconds(SelectorSleepTime);
        _selectorAwake = true;
    }

    #region Traversal methods
    private Battler MoveUp()
    {
        var curY = TargetedBattler.transform.position.y;
        var curX = TargetedBattler.transform.position.x;
        List<Battler> closestAboveBattlers = new List<Battler>();
        foreach(Battler battler in _battlers)
        {
            // if the battler is above the current battler
            if(battler.transform.position.y > curY)
            {
                // if we've already found a battler above the TargetedBattler...
                if (closestAboveBattlers.Count > 0)
                {
                    // if this battler is just as far left/right, add it to the list (we will return whichever has the closest Y value later)
                    if(Utils.FloatsApproxEqual(battler.transform.position.x, closestAboveBattlers[0].transform.position.x))
                    {
                        closestAboveBattlers.Add(battler);
                    }
                    // if this battler is closer to the Targeted battler, reset the list and add it
                    else if(Mathf.Abs(battler.transform.position.x - curX) < Mathf.Abs(closestAboveBattlers[0].transform.position.x - curX))
                    {
                        closestAboveBattlers.Clear();
                        closestAboveBattlers.Add(battler);
                    }
                }
                // if the list is empty, just add this battler to the list
                else
                {
                    closestAboveBattlers.Add(battler);
                }
            }
        }

        if(closestAboveBattlers.Count == 0)
        {
            // Didn't find any battlers above the currently Targeted one
            return TargetedBattler;
        }

        Battler result = closestAboveBattlers[0];

        // get the closest above battler of the ones we've found with close X coords
        for (int i = 1; i < closestAboveBattlers.Count; i++)
        {
            if((Mathf.Abs(closestAboveBattlers[i].transform.position.y - curY)) < Mathf.Abs(result.transform.position.y - curY))
            {
                result = closestAboveBattlers[i];
            }
        }

        return result;
    }

    private Battler MoveDown()
    {
        var curY = TargetedBattler.transform.position.y;
        var curX = TargetedBattler.transform.position.x;
        List<Battler> closestBelowBattlers = new List<Battler>();
        foreach (Battler battler in _battlers)
        {
            // if the battler is below the current battler
            if (battler.transform.position.y < curY)
            {
                // if we've already found a battler below the TargetedBattler...
                if (closestBelowBattlers.Count > 0)
                {
                    // if this battler is just as far right/left, add it to the list (we will return whichever has the closest Y value later)
                    if (Utils.FloatsApproxEqual(battler.transform.position.x, closestBelowBattlers[0].transform.position.x))
                    {
                        closestBelowBattlers.Add(battler);
                    }
                    // if this battler is closer to the Targeted battler, reset the list and add it
                    else if (Mathf.Abs(battler.transform.position.x - curX) < Mathf.Abs(closestBelowBattlers[0].transform.position.x - curX))
                    {
                        closestBelowBattlers.Clear();
                        closestBelowBattlers.Add(battler);
                    }
                }
                // if the list is empty, just add this battler to the list
                else
                {
                    closestBelowBattlers.Add(battler);
                }
            }
        }

        if (closestBelowBattlers.Count == 0)
        {
            // Didn't find any battlers below the currently Targeted one
            return TargetedBattler;
        }

        Battler result = closestBelowBattlers[0];

        // get the closest below battler of the ones we've found with close X coords
        for (int i = 1; i < closestBelowBattlers.Count; i++)
        {
            if ((Mathf.Abs(closestBelowBattlers[i].transform.position.y - curY)) < Mathf.Abs(result.transform.position.y - curY))
            {
                result = closestBelowBattlers[i];
            }
        }

        return result;
    }

    private Battler MoveRight()
    {
        var curY = TargetedBattler.transform.position.y;
        var curX = TargetedBattler.transform.position.x;
        List<Battler> closestRightBattlers = new List<Battler>();
        foreach (Battler battler in _battlers)
        {
            // if the battler is to the right of the current battler
            if (battler.transform.position.x > curX && !Utils.FloatsApproxEqual(battler.transform.position.x, curX))
            {
                // if we've already found a battler to the right of the TargetedBattler...
                if (closestRightBattlers.Count > 0)
                {
                    // if this battler is just as far up/down, add it to the list (we will return whichever has the closest X value later)
                    if (Utils.FloatsApproxEqual(battler.transform.position.y, closestRightBattlers[0].transform.position.y))
                    {
                        closestRightBattlers.Add(battler);
                    }
                    // if this battler is closer to the Targeted battler, reset the list and add it
                    else if (Mathf.Abs(battler.transform.position.y - curY) < Mathf.Abs(closestRightBattlers[0].transform.position.y - curY))
                    {
                        closestRightBattlers.Clear();
                        closestRightBattlers.Add(battler);
                    }
                }
                // if the list is empty, just add this battler to the list
                else
                {
                    closestRightBattlers.Add(battler);
                }
            }
        }

        if (closestRightBattlers.Count == 0)
        {
            // Didn't find any battlers to the right of the currently Targeted one
            return TargetedBattler;
        }

        Battler result = closestRightBattlers[0];

        // get the closest right battler of the ones we've found with close Y coords
        for (int i = 1; i < closestRightBattlers.Count; i++)
        {
            if ((Mathf.Abs(closestRightBattlers[i].transform.position.x - curX)) < Mathf.Abs(result.transform.position.x - curX))
            {
                result = closestRightBattlers[i];
            }
        }

        return result;
    }

    private Battler MoveLeft()
    {
        var curY = TargetedBattler.transform.position.y;
        var curX = TargetedBattler.transform.position.x;
        List<Battler> closestLeftBattlers = new List<Battler>();
        foreach (Battler battler in _battlers)
        {
            // if the battler is to the left of the current battler
            if (battler.transform.position.x < curX)
            {
                // if we've already found a battler to the left of the TargetedBattler...
                if (closestLeftBattlers.Count > 0)
                {
                    // if this battler is just as far up/down, add it to the list (we will return whichever has the closest X value later)
                    if (Utils.FloatsApproxEqual(battler.transform.position.y, closestLeftBattlers[0].transform.position.y))
                    {
                        closestLeftBattlers.Add(battler);
                    }
                    // if this battler is closer to the Targeted battler, reset the list and add it
                    else if (Mathf.Abs(battler.transform.position.y - curY) < Mathf.Abs(closestLeftBattlers[0].transform.position.y - curY))
                    {
                        closestLeftBattlers.Clear();
                        closestLeftBattlers.Add(battler);
                    }
                }
                // if the list is empty, just add this battler to the list
                else
                {
                    closestLeftBattlers.Add(battler);
                }
            }
        }

        if (closestLeftBattlers.Count == 0)
        {
            // Didn't find any battlers to the left of the currently Targeted one
            return TargetedBattler;
        }

        Battler result = closestLeftBattlers[0];

        // get the closest left battler of the ones we've found with close Y coords
        for (int i = 1; i < closestLeftBattlers.Count; i++)
        {
            if ((Mathf.Abs(closestLeftBattlers[i].transform.position.x - curX)) < Mathf.Abs(result.transform.position.x - curX))
            {
                result = closestLeftBattlers[i];
            }
        }

        return result;
    }

    #endregion
}
