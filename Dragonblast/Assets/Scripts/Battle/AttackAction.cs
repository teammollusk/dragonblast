﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class AttackAction : Action
{
    private Element[] _damageType;

    public AttackAction(Battler user)
    {
        User = user;
    }

    public AttackAction(Battler user, Element[] damageType)
    {
        User = user;
        _damageType = damageType;
    }

    public override void Execute()
    {
        User.StartCoroutine(User.MoveAndAttack(Target, this, _damageType));
    }

    public override void Undo()
    {
        Target.LoseHealth(-1 * User.Damage, _damageType);
    }
}
