﻿using UnityEngine;
using System.Collections;

public class BurritoMonsterDeath : MonoBehaviour, IEnemyDeath
{
	public void Execute()
    {
        EndGame.Instance.Execute();
    }
}
