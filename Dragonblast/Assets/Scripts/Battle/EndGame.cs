﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour
{
	public static EndGame Instance;

    private const string GameOverText = "Thus Ends Our Epic Tale";
    private const float TextSpeed = .15f;

    public void Awake()
    {
		if (Instance)
			Debug.LogError("Attempted to create second instance of a singleton");

		Instance = this;
	}

	public void Execute()
	{
		StartCoroutine(EndGameRoutine());
	}

	private IEnumerator EndGameRoutine()
    {
        var player = GameObject.FindGameObjectWithTag(Constants.PlayerTag).GetComponent<Player>();//.DisableMovement();
        player.DisableMovement();
        yield return new WaitForSeconds(.5f);
        player.StartPartyMemberDialogue(PartyMemberName.Brett, "Brett|That was pretty nuts.;Justin|Yup.");
        yield return new WaitForSeconds(1f);
        yield return new WaitUntil(() => TheGame.GetState() != GameState.Dialogue);
        yield return FadeIn();
    }

    private IEnumerator FadeIn()
    {
        var image = GetComponent<Image>();
        var gameOverText = transform.Find("GameOverText").GetComponent<Text>();
        var continueText = transform.Find("ContinueText").GetComponent<Text>();
        var r = image.color.r;
        var g = image.color.g;
        var b = image.color.b;
        yield return new WaitForSeconds(.8f);
        while (image.color.a < 1)
        {
            image.color = new Color(r, g, b, image.color.a + .02f);
            yield return new WaitForSeconds(.01f);
        }
        yield return new WaitForSeconds(.5f);
        yield return TextifyCanvas(gameOverText);
        yield return new WaitForSeconds(.5f);
        continueText.enabled = true;
    }

    private IEnumerator TextifyCanvas(Text textComponent)
    {
        textComponent.text = "";
        while (textComponent.text.Length < GameOverText.Length)
        {
            if (textComponent != null)
            {
                var newText = GameOverText.Substring(0, textComponent.text.Length + 1);
                yield return new WaitForSeconds(TextSpeed);
                if(newText.EndsWith(" "))
                {
                    yield return new WaitForSeconds(TextSpeed);
                }
                textComponent.text = newText;
            }
            else
            {
                break;
            }
        }
    }
}
