﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleInitializer : MonoBehaviour
{
    public static BattleInitializer Instance;

    private Vector3 _battlerPositionsOffsetVector;

    private GameObject _playerGameObject;
    private Vector3 _playerStartPosition;
    private Vector3 _playerRunDir;
    private Vector3[] _allyBattlerPositions = new Vector3[Constants.BattlePartySize];
    private int _allyBattlerPositionsSelected;

    private EnemyPartyMember[] _enemyPartyMembers;
    private Vector3 _enemyStartPosition;
    private string _enemyFacingDirection;
    private Vector3[] _enemyBattlerPositions = new Vector3[Constants.BattlePartySize];
    private int _enemyBattlerPositionsSelected;
    private Enemy _enemy;

    private bool _playerWeaponHit;

    private Vector3 _battleCenter;

    Battler[] _battlers = new Battler[Constants.BattlePartySize * 2];
    AllyBattler[] _allyBattlers = new AllyBattler[Constants.BattlePartySize];
    EnemyBattler[] _enemyBattlers = new EnemyBattler[Constants.BattlePartySize];


    void Awake()
    {
		if (Instance)
			Debug.LogError("Attempted to create second instance of a singleton");

		Instance = this;
	}

    /// <summary>
    /// Sole entry point. Initializes a battle.
    /// </summary>
    /// <param name="enemyGameObject"></param>
    /// <param name="playerGameObject"></param>
    /// <param name="playerWeaponHit"></param>
    public void Execute(GameObject enemyGameObject, GameObject playerGameObject, bool playerWeaponHit)
    {
        Reset();

        // Initialize ally variables
        Vector3 playerFacingDir;
        _playerGameObject = playerGameObject;
        _playerStartPosition = playerGameObject.transform.position;
        Debug.Log("Player position when battle was initiated: " + _playerStartPosition.ToString("F8"));
        var allyFacingDirection = Utils.GetFacingDirection(playerGameObject.transform.position, enemyGameObject.transform.position, out playerFacingDir);
        _playerWeaponHit = playerWeaponHit;

        // Initialize enemy variables
        _enemyPartyMembers = enemyGameObject.GetComponent<EnemyParty>().PartyMembers;
        _enemyStartPosition = enemyGameObject.transform.position;
        Debug.Log("Enemy position when battle was initiated: " + _enemyStartPosition.ToString("F8"));
        _enemyFacingDirection = Utils.GetFacingDirection(enemyGameObject.transform.position, playerGameObject.transform.position);
        _enemy = enemyGameObject.GetComponent<Enemy>();

        // Rotate the "away from enemy" vector 90 degrees to get the default ally battler axis
        _battlerPositionsOffsetVector = (Quaternion.AngleAxis(90, Vector3.forward) * playerFacingDir * -1) * Constants.PersonalBubbleDiameter;
        _playerRunDir = playerFacingDir * -1;

        // Position battlers
        SetFirstBattlerPositions();
        _allyBattlerPositionsSelected = SetBattlerPositions(_allyBattlerPositions, _playerRunDir, TheParty.GetMembers().Count, _playerStartPosition, _enemyBattlerPositions, 1);
        _enemyBattlerPositionsSelected = SetBattlerPositions(_enemyBattlerPositions, _playerRunDir * -1, _enemyPartyMembers.Length, _enemyStartPosition, _allyBattlerPositions, _allyBattlerPositionsSelected);
        _battleCenter = GetBattleCenter();

        // Create Battlers
        var allyPartyMembers = TheParty.GetMembers();
        for (int i = 0; i < _allyBattlerPositionsSelected; i++)
            _battlers[i] = _allyBattlers[i] = AllyBattler.Create(allyPartyMembers[i], _playerStartPosition, _allyBattlerPositions[i], allyFacingDirection, _playerWeaponHit);
        for (int i = 0; i < _enemyBattlerPositionsSelected; i++)
            _battlers[i + _allyBattlerPositionsSelected] = _enemyBattlers[i] = EnemyBattler.Create(_enemyPartyMembers[i], _enemyStartPosition, _enemyBattlerPositions[i], _enemyFacingDirection, _playerWeaponHit);

        StartCoroutine(SetupBattleDisplay());
    }

    /// <summary>
    /// Reset the things that get reused from battle to battle
    /// </summary>
    private void Reset()
    {
        Array.Clear(_allyBattlerPositions, 0, _allyBattlerPositions.Length);
        Array.Clear(_enemyBattlerPositions, 0, _enemyBattlerPositions.Length);
        Array.Clear(_battlers, 0, _battlers.Length);
        Array.Clear(_allyBattlers, 0, _allyBattlers.Length);
        Array.Clear(_enemyBattlers, 0, _enemyBattlers.Length);
    }

    /// <summary>
    /// Set up battle UI, etc
    /// </summary>
    /// <returns></returns>
    private IEnumerator SetupBattleDisplay()
    {
        // Pan camera
        yield return StartCoroutine(_playerGameObject.GetComponent<Player>().PanToPosition(_battleCenter));

        // Create Timeline
        var timeline = Timeline.Create(_battlers);

        DisplayWeaponHitText(timeline.CanvasTransform);

        // Init Battle Manager
        BattleManager.Instance.StartBattle(timeline, _allyBattlers, _enemyBattlers, _playerGameObject, _enemy, _battleCenter);
    }

    /// <summary>
    /// Average the positions of all battlers to get the center of the battlefield
    /// Used for deciding where the camera pans to and where the timeline spawns
    /// </summary>
    /// <returns></returns>
    private Vector3 GetBattleCenter()
    {
        var center = Vector3.zero;
        for(int i = 0; i < _allyBattlerPositionsSelected; i++)
        {
            center += _allyBattlerPositions[i];
        }
        for(int i = 0; i < _enemyBattlerPositionsSelected; i++)
        {
            center += _enemyBattlerPositions[i];
        }
        center /= (_allyBattlerPositionsSelected + _enemyBattlerPositionsSelected);
        return center;
    }

    /// <summary>
    /// If there's a wall in the way, set the initial Battler position to right before the wall
    /// </summary>
    /// <param name="destination"></param>
    /// <returns></returns>
    private bool AdjustForCollisions(ref Vector3 destination, Vector3 startPosition)
    {
        var raycastDirection = destination - startPosition;
        RaycastHit2D raycast = Physics2D.CircleCast(startPosition, Constants.RaycastWidthDefault, raycastDirection, raycastDirection.magnitude, Constants.DefaultOnlyLayerMask);
        if (raycast.collider != null)
        {
            Vector2 offset = raycast.normal.normalized * Constants.BattlerWidth / 2;
            destination = raycast.point + offset;
            return true;
        }
        return false;
    }

    /// <summary>
    /// Find positions for the first ally and first enemy battler. All other battler positions will be based on these.
    /// </summary>
    private void SetFirstBattlerPositions()
    {
        _allyBattlerPositions[0] = _enemyStartPosition + _playerRunDir * Constants.AllyBattlerDistanceFromEnemy;
        bool allyPositionAdjusted = AdjustForCollisions(ref _allyBattlerPositions[0], _playerStartPosition);
        if (allyPositionAdjusted)
        {
            _enemyBattlerPositions[0] = _allyBattlerPositions[0] - _playerRunDir * Constants.AllyBattlerDistanceFromEnemy;
            AdjustForCollisions(ref _enemyBattlerPositions[0], _enemyStartPosition);
        }
        else
        {
            _enemyBattlerPositions[0] = _enemyStartPosition;
        }
    }

    /// <summary>
    /// Find positions for your allies by iterative-recursively checking to the left, right, and back of the starting position
    /// </summary>
    /// <param name="positionArray">The resulting positions</param>
    /// <param name="backwardsDirection">Which way is away from the enemies?</param>
    /// <param name="partySize">Number of positions to attempt to set total</param>
    /// <param name="startPosition">Where the player was when combat was initiated, and where all the party members will be running from</param>
    /// <param name="opposingPositions">Every position in this array needs a clear line of sight to X for X to be a valid position</param>
    /// <param name="opposingPositionsLength">Cuz I'm using arrays instead of List<>'s for some reason</param>
    /// <returns></returns>
    private int SetBattlerPositions(Vector3[] positionArray, Vector3 backwardsDirection, int partySize, Vector3 startPosition, Vector3[] opposingPositions, int opposingPositionsLength)
    {
        HashSet<Vector3> selectedPositions = new HashSet<Vector3>() { positionArray[0] };

        // Try to position battlers to the left and right of the original
        List<Vector3> positionsToCheck = new List<Vector3>() { positionArray[0] - _battlerPositionsOffsetVector, positionArray[0] + _battlerPositionsOffsetVector };//, positionArray[0] + backwardsDirection * _battlerPositionsOffet };
        List<Vector3> surroundingOffsetsToCheck = new List<Vector3>() { -1 * _battlerPositionsOffsetVector, _battlerPositionsOffsetVector };
        CheckPositions(ref positionArray, positionsToCheck, surroundingOffsetsToCheck, partySize, startPosition, opposingPositions, opposingPositionsLength, selectedPositions);

        // If that fails, try putting them behind the placed battlers
        positionsToCheck.Clear();
        foreach(Vector3 position in selectedPositions)
        {
            positionsToCheck.Add(position + backwardsDirection * Constants.PersonalBubbleDiameter);
        }
        surroundingOffsetsToCheck.Add(backwardsDirection * Constants.PersonalBubbleDiameter);
        CheckPositions(ref positionArray, positionsToCheck, surroundingOffsetsToCheck, partySize, startPosition, opposingPositions, opposingPositionsLength, selectedPositions);

        //TODO: Handle this
        if (selectedPositions.Count < partySize)
        {
            Debug.LogError(string.Format("Only {0} {1} could be placed. Make this area less narrow.", selectedPositions.Count, backwardsDirection == _playerRunDir ? "allies" : "enemies"));
        }

        return selectedPositions.Count;
    }

    /// <summary>
    /// Helper method for SetBattlerPositions
    /// </summary>
    /// <param name="positionArray">see SetBattlerPositions</param>
    /// <param name="positionsToCheck">see SetBattlerPositions</param>
    /// <param name="surroundingOffsetsToCheck">If a position in positionsToCheck works, then also check each of these offsets added to it</param>
    /// <param name="partySize">see SetBattlerPositions</param>
    /// <param name="startPosition">see SetBattlerPositions</param>
    /// <param name="opposingPositions">see SetBattlerPositions</param>
    /// <param name="opposingPositionsLength">see SetBattlerPositions</param>
    /// <param name="selectedPositions">see SetBattlerPositions</param>
    private void CheckPositions(ref Vector3[] positionArray, List<Vector3> positionsToCheck, List<Vector3> surroundingOffsetsToCheck, int partySize, Vector3 startPosition, Vector3[] opposingPositions, int opposingPositionsLength, HashSet<Vector3> selectedPositions)
    {
        for (int i = 0; i < positionsToCheck.Count && selectedPositions.Count < partySize; i++)
        {
            // Check if there's a clear line of sight from this ally position to all enemy positions (or vice versa)
            bool canSeeEnemies = true;
            for (int j = 0; j < opposingPositionsLength; j++)
            {
                if (Utils.IsObstructed(positionsToCheck[i], opposingPositions[j], Constants.DefaultOnlyLayerMask, Constants.RaycastWidthDefault))
                {
                    canSeeEnemies = false;
                    break;
                }
            }

            // If a good position its found, proceed to check the positions around it
            if (canSeeEnemies && !PositionAlreadySelected(selectedPositions, positionsToCheck[i]) && !Utils.IsObstructed(startPosition, positionsToCheck[i], Constants.DefaultOnlyLayerMask, Constants.RaycastWidthDefault))
            {
                positionArray[selectedPositions.Count] = positionsToCheck[i];
                selectedPositions.Add(positionsToCheck[i]);

                foreach(Vector3 offset in surroundingOffsetsToCheck)
                {
                    positionsToCheck.Add(positionsToCheck[i] + offset);
                }
            }
        }
    }

    /// <summary>
    /// Used in lieu of HashSet.Contains because trying to write a hash function that handles approximate Vector3 equivilance is more trouble than it's worth
    /// Especially when the max of n is 2
    /// </summary>
    /// <param name="selectedPositions"></param>
    /// <param name="positionToCheck"></param>
    /// <returns></returns>
    private bool PositionAlreadySelected(HashSet<Vector3> selectedPositions, Vector3 positionToCheck)
    {
        float minDistance = Constants.PersonalBubbleDiameter * 0.5f;
        foreach (Vector3 pos in selectedPositions)
        {
            if (Vector3.Distance(pos, positionToCheck) < minDistance)
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Display Hit! in green or Miss! in red depending on which happened. Should probably move this. 
    /// </summary>
    /// <param name="canvasTransform"></param>
    private void DisplayWeaponHitText(RectTransform canvasTransform)
    {
        if (_playerWeaponHit)
        {
            FloatingCombatText.Create(_playerStartPosition, "Hit!", canvasTransform, 0f, 1f, 0f);
        }
        else
        {
            FloatingCombatText.Create(_playerStartPosition, "Miss!", canvasTransform, 1f, 0f, 0f);
        }
    }
}
