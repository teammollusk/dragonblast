﻿using UnityEngine;
using System.Collections;
using System;

public class FocusSkill : Skill
{
    const int Duration = 4;

    public override void Execute()
    {
        if(!User.IsStatusEffectActive("Focus"))
        {
            User.Speed *= 2;
        }
        User.AddStatusEffect("Focus", Duration);
        User.FinishAction();
    }

    public override void Undo()
    {
        User.Speed /= 2;
    }

    public override void SetSkillDescription()
    {
        Description = "Doubles the user's speed for 3 turns";
    }
}
