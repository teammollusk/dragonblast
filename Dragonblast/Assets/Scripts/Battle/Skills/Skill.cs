﻿using UnityEngine;
using System.Collections;
using System;

public abstract class Skill : Action {

    public string Description { get; set; }

    public Skill()
    {
        SetSkillDescription();
    }

    public abstract override void Execute();

    public abstract override void Undo();

    public abstract void SetSkillDescription();

}
