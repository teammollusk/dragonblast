﻿using UnityEngine;
using System.Collections;
using System;

public class YellSkill : Skill {

    public override void Execute()
    {
        FloatingCombatText.Create(User.transform.position, "AAAAAAHHHHHHH!!!", User.TimelineTransform);
        User.FinishAction();
    }

    public override void Undo()
    {
        FloatingCombatText.Create(User.transform.position, "!!!HHHHHHHAAAAAA", User.TimelineTransform);
        User.FinishAction();
    }

    public override void SetSkillDescription()
    {
        Description = "Create a loud sound using your voice";
    }
}
