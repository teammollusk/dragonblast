﻿using UnityEngine;
using System.Collections;
using System;

public class SuicideSkill : Skill
{
    public override void Execute()
    {
        User.Damage = 9999;
        var attackAction = new AttackAction(User);
        attackAction.Target = User;
        attackAction.Execute();
    }

    public override void Undo()
    {
        throw new NotImplementedException();
    }

    public override void SetSkillDescription()
    {
        Description = "Self explanatory";
    }
}
