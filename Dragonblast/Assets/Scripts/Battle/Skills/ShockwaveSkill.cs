﻿using UnityEngine;
using System.Collections;
using System;

public class ShockwaveSkill : Skill
{
    private const float _shockwaveSpeed = 6f;
    private const int _damageDenominator = 15;
    public override void Execute()
    {
        User.StartCoroutine(SendShockwaves());
    }

    private IEnumerator SendShockwaves()
    {
        var startPosition = User.transform.position;
        yield return User.MoveTo(BattleManager.Instance.BattleCenter);
        User.AnimateAttackNoWait();
        yield return new WaitForSeconds(.5f);
        foreach (var enemy in BattleManager.Instance.Enemies)
        {
            if (enemy != null)
            {
				var shockwave = TheObjectPool.Get(PooledObjects.Shockwave);
				shockwave.transform.SetPositionAndRotation(User.transform.position, User.transform.rotation);
				shockwave.SetActive(true);

				var distanceVector = enemy.transform.position - User.transform.position;
                distanceVector /= distanceVector.magnitude;
                shockwave.transform.up = distanceVector;
                var shockwaveVelocity = distanceVector * _shockwaveSpeed;
                shockwave.GetComponent<Rigidbody2D>().velocity = shockwaveVelocity;
                enemy.ChangePosition(-20);
            }
        }
        yield return new WaitForSeconds(.25f);
        foreach (var enemy in BattleManager.Instance.Enemies)
        {
            if(enemy != null)
            {
                enemy.LoseHealth(User.Damage / _damageDenominator);
            }
        }
        yield return new WaitForSeconds(.5f);
        yield return User.MoveTo(startPosition);
        User.FinishAction();
    }

    public override void Undo()
    {
        Target.LoseHealth(-1 * User.Damage / 30);
    }

    public override void SetSkillDescription()
    {
        Description = "Damages and knocks back all enemies";
    }
}
