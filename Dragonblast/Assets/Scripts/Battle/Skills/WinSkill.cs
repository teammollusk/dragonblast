﻿using UnityEngine;
using System.Collections;
using System;

public class WinSkill : Skill
{
    public override void Execute()
    {
        foreach(var enemy in BattleManager.Instance.Enemies)
        {
            if (enemy != null)
                enemy.LoseHealth(9999);
        }
        User.FinishAction();
    }

    public override void SetSkillDescription()
    {
        Description = "Hax";
    }

    public override void Undo()
    {
        throw new NotImplementedException();
    }
}