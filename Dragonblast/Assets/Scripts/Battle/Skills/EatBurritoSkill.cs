﻿using UnityEngine;
using System.Collections;
using System;

public class EatBurritoSkill : Skill
{
    public override void Execute()
    {
        var firstEnemy = BattleManager.Instance.Enemies[0];
        if (firstEnemy.Name.Equals("Homewrecker"))
        {
            User.StartCoroutine(EatBurrito(firstEnemy));
        }
        else
        {
            FloatingCombatText.Create(User.transform.position, "That's not a burrito", User.TimelineTransform);
            User.FinishAction();
        }
    }

    public override void SetSkillDescription()
    {
        Description = "Eat the largest nearby burrito";
    }

    private IEnumerator EatBurrito(Battler target)
    {
        yield return User.MoveNextTo(target);
        yield return new WaitForSeconds(.3f);
        User.LoseHealth(-10);
        target.LoseHealth(10);
        target.Defend();
        yield return new WaitForSeconds(.3f);
        target.UnDefend();
        yield return User.MoveBack();
        User.FinishAction();
    }

    public override void Undo()
    {
        throw new NotImplementedException();
    }
}