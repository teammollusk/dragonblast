﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class EnemyBattler : Battler
{
    /// <summary>
    /// Get a generic battler prefab from the object pool, add an EnemyBattler script to it, and initialize its params
    /// </summary>
    /// <param name="battlerPrefab"></param>
    /// <param name="partyMember"></param>
    /// <param name="startPosition"></param>
    /// <param name="destination"></param>
    /// <param name="facingDirection"></param>
    /// <returns></returns>
    public static EnemyBattler Create(PartyMember partyMember, Vector3 startPosition, Vector3 destination, string facingDirection, bool playerWeaponHit)
    {
        GameObject battlerGameObject = TheObjectPool.Get(PooledObjects.Battler);
        battlerGameObject.transform.SetPositionAndRotation(startPosition, Quaternion.identity);
        EnemyBattler eb = battlerGameObject.AddComponent<EnemyBattler>();
        battlerGameObject.tag = Constants.EnemyBattlerTag;

        Init(eb, battlerGameObject, partyMember, destination, facingDirection, !playerWeaponHit);

        // Initialize Enemy specific params here
        // Move the sprite up or down in the sorting order depending on position
        var spriteRenderer = battlerGameObject.GetComponent<SpriteRenderer>();
        if (facingDirection == Constants.Up)
        {
            spriteRenderer.sortingLayerName = Constants.BattlerAboveLayer;
        }
        else
        {
            spriteRenderer.sortingLayerName = Constants.BattlerBelowLayer;
        }

        battlerGameObject.SetActive(true);
        return eb;
    }

    /// <summary>
    /// Decide which action to take during combat based upon enemy and ally details
    /// </summary>
    /// <returns> Returns an action for BattleManager's CombatUpdate() </returns>
    public override void ChooseCombatAction()
    {
        //setup the attack action, choose which target to hit, perform the attack
        AttackAction _enemyAction = new AttackAction(this);
        AllyBattler _target = this.FindLowHPAllyBattler(BattleManager.Instance.Allies);
        this.BasicAttack(_enemyAction, _target);
    }

    /// <summary>
    /// Find the ally battler with the lowest amount of health points for use with combat AI
    /// </summary>
    /// <param name="allies"> The array of allies the enemies are facing </param>
    /// <returns>
    /// The ally battler with the lowest amount of health points
    /// </returns>
    private AllyBattler FindLowHPAllyBattler(AllyBattler[] allies)
    {
        AllyBattler lowestAllyBattler = null;
        int HPfinder = 999;
        for(int i = 0; i < allies.Length; i++)
        {
            if(allies[i] != null && allies[i].gameObject.activeInHierarchy)
            {
                if (allies[i].Health < HPfinder)
                {
                    lowestAllyBattler = allies[i];
                    HPfinder = lowestAllyBattler.Health;
                }
            }
        }
        return lowestAllyBattler;
    }

    /// <summary>
    /// Perform a basic attack on a pre-determined target
    /// </summary>
    /// <param name="attackAction"> The type of action the enemy wants to use for a basic attack on an ally </param>
    /// <param name="target"> The specific ally this enemy wants to attack </param>
    /// <returns></returns>
    private void BasicAttack(AttackAction attackAction, AllyBattler target)
    {
        attackAction.Target = target;
        attackAction.Execute();
    }
}
