﻿using UnityEngine;
using System.Collections;

public class DialogueTriggerZone : MonoBehaviour {

    [SerializeField]
    private string _dialogue = default;
    [SerializeField]
    private bool _disableOnTrigger = true;

    void OnTriggerEnter2D(Collider2D c)
    {
        if(c.tag.Equals(Constants.PlayerTag))
        {
            SourcelessDialogue.Create(_dialogue, GetComponent<IDialogueEvent>());
            if (_disableOnTrigger)
            {
                GetComponent<Collider2D>().enabled = false;
            }
        }
    }
}
