﻿using UnityEngine;
using System.Collections;

public class SourcelessDialogue : Dialogue {

    private IDialogueEvent _dialogueEvent;

    public static GameObject Create(string dialogue, IDialogueEvent dialogueEvent = null)
    {
		GameObject sdObject = TheObjectPool.Get(PooledObjects.SourcelessDialogue);
		sdObject.SetActive(true);
		var sd = sdObject.GetComponent<SourcelessDialogue>();
        sd._dialogueEvent = dialogueEvent;
        sd.SetPhantomTarget();
        sd.UpdateDialogueSequence(new string[] { dialogue }, 0);
        sd.StartDialogue();
        return sdObject;
    }

    protected override void EndDialogue()
    {
        base.EndDialogue();
        if(_dialogueEvent != null)
            _dialogueEvent.Execute();

		gameObject.SetActive(false);
    }
}
