﻿using UnityEngine;
using System.Collections;
using System;

public class DialogueEvent_EnableScript : MonoBehaviour, IDialogueEvent
{
    [SerializeField]
    private MonoBehaviour _script = default;
    [SerializeField]
    private bool _disableInstead = false;

    public void Execute()
    {
        _script.enabled = !_disableInstead;
    }
}