﻿using UnityEngine;
using System.Collections;

public class DialogueEvent_AddBrett : DialogueEvent_AddPartyMember
{
    [SerializeField]
    private GameObject _burritoMonster = default;

    protected override IEnumerator CharacterSpecificEvents()
    {
        Instantiate(_burritoMonster, _burritoMonster.transform.position, _burritoMonster.transform.rotation);
        yield return null;
    }
}
