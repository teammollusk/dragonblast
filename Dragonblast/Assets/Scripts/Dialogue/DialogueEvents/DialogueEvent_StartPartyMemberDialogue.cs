﻿using UnityEngine;
using System.Collections;
using System;

public class DialogueEvent_StartPartyMemberDialogue : MonoBehaviour, IDialogueEvent
{
    [SerializeField]
    Player _player = default;
    [SerializeField]
    string _dialogue = default;

    public void Execute()
    {
        _player.StartPartyMemberDialogue(PartyMemberName.Scott, _dialogue);
    }
}