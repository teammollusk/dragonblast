﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System;

public class DialogueEvent_Respawn : MonoBehaviour, IDialogueEvent {

    public void Execute()
    {
        GameObject.FindObjectOfType<Player>().Respawn();
    }
}
