﻿using UnityEngine;
using System.Collections;
using System;

public class DialogueEvent_DisableDialogue : MonoBehaviour, IDialogueEvent
{
    public void Execute()
    {
        GetComponent<Dialogue>().Disable();
    }
}