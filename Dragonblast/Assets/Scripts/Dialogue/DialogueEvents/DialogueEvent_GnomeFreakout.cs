﻿using UnityEngine;
using System.Collections;
using System;

public class DialogueEvent_GnomeFreakout : MonoBehaviour, IDialogueEvent {

    [SerializeField]
    private Player _player = default;
    [SerializeField]
    private Enemy _enemy = default;
	[SerializeField]
	private CombatInitiationFirstGnome _intiationScript = default;

    //private const string Dialogue = "Justin|That garden gnome's gonna charge us.;Scott|Wha" + Constants.DialogueAutoAdvance;
    private readonly string Dialogue = "Scott|What?;Justin|That gnome just looked at me.;Scott|Seriously?;Justin|Yeah, I have a bad feeling about this.;Scott|No I mean, seriously, you think a garden gnome looked at you?;Justin|Here it comes!" + Constants.DialogueAutoAdvance;

    public void Execute()
    {
		_intiationScript.DialogueTriggered = true;
        _player.StartPartyMemberDialogue(PartyMemberName.Scott, Dialogue, DialogueEvent_ActivateEnemy.Create(_enemy, _player));
    }    
}
