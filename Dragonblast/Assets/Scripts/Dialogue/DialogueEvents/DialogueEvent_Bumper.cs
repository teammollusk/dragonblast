﻿using UnityEngine;
using System.Collections;
using System;

public class DialogueEvent_Bumper : MonoBehaviour, IDialogueEvent {

    [SerializeField]
    private Vector2 _bumpVector = default;
    private Vector2 _playerPosition;
    private Player _player;

    public void Execute()
    {
        var facingDir = Utils.GetFacingDirection(Vector2.zero, _bumpVector);
        _player.AutoMoveTo(_playerPosition + _bumpVector, facingDir);
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if(c.tag.Equals(Constants.PlayerTag))
        {
            _playerPosition = c.transform.position;
            _player = c.GetComponent<Player>();
        }
    }
}
