﻿using UnityEngine;
using System.Collections;
using System;

public class DialogueEvent_SetDialogue : MonoBehaviour, IDialogueEvent
{
    [SerializeField]
    private Dialogue _dialogueComponent = default;
    [SerializeField]
    private string _dialogueString = default;

    public void Execute()
    {
        _dialogueComponent.UpdateDialogueSequence(new string[] { _dialogueString }, 0);
    }
}