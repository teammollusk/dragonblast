﻿using UnityEngine;
using System.Collections;
using System;

public class DialogueEvent_AddPartyMember : MonoBehaviour, IDialogueEvent {

    [SerializeField]
    private AllyPartyMember _partyMember = default;
    [SerializeField]
    private Transform _playerTransform = default;

    public void Execute()
    {
        if (!enabled) return;

        StartCoroutine(AddPartyMemberRoutine());
    }

    private IEnumerator AddPartyMemberRoutine()
    {
        GetComponent<Collider2D>().enabled = false;
        GetComponent<Dialogue>().Disable();
        var playerScript = _playerTransform.GetComponent<Player>();
        playerScript.DisableMovement();
        var partyMemberMovement = GetComponent<MovementController>();
        partyMemberMovement.AutoMoveTo(_playerTransform.position);

        yield return new WaitUntil(() => !partyMemberMovement.IsAutoMoving);

        TheParty.AddMember(_partyMember);
        SourcelessDialogue.Create(_partyMember.Name + " joined the party!");
        playerScript.EnableMovement();

        yield return CharacterSpecificEvents();

        Destroy(gameObject);
    }

    protected virtual IEnumerator CharacterSpecificEvents()
    {
        // Override in character specific subclasses
        yield return null;
    }
}
