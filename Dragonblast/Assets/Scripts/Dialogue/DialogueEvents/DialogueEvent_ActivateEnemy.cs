﻿using UnityEngine;
using System.Collections;
using System;

public class DialogueEvent_ActivateEnemy : MonoBehaviour, IDialogueEvent {

    private Player _player;
    private Enemy _enemy;

    public static DialogueEvent_ActivateEnemy Create(Enemy enemy, Player player)
    {
        GameObject aedeObject = TheObjectPool.Get(PooledObjects.ActivateEnemyDialogueEvent);
		aedeObject.SetActive(true);
        var aede = aedeObject.GetComponent<DialogueEvent_ActivateEnemy>();
        aede._player = player;
        aede._enemy = enemy;
        return aede;
    }

    public void Execute()
    {
        _enemy.Activate();
        var facingDirection = Utils.GetFacingDirection(_player.transform.position, _enemy.transform.position);
        _player.Face(facingDirection);
		gameObject.SetActive(false);
    }

}
