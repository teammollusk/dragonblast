﻿using UnityEngine;
using System.Collections;
using System;

public class DialogueEvent_Disappear : MonoBehaviour, IDialogueEvent
{
    public void Execute()
    {
        Destroy(gameObject);
    }
}