﻿using UnityEngine;
using System.Collections;

public interface IDialogueEvent
{
    void Execute();
}
