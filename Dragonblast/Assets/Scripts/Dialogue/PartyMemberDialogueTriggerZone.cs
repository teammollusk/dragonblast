﻿using UnityEngine;
using System.Collections;

public class PartyMemberDialogueTriggerZone : MonoBehaviour
{
    [SerializeField]
    private string _dialogue = default;
    [SerializeField]
    private bool _disableOnTrigger = true;
    [SerializeField]
    private PartyMemberName _partyMemberName = default;

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.tag.Equals(Constants.PlayerTag))
        {
            var player = c.GetComponent<Player>();
            player.StartPartyMemberDialogue(_partyMemberName, _dialogue);
            if (_disableOnTrigger)
            {
                GetComponent<Collider2D>().enabled = false;
            }
        }
    }
}