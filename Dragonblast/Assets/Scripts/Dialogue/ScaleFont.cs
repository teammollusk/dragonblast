﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[ExecuteInEditMode]
public class ScaleFont : MonoBehaviour
{
    [SerializeField]
    private float _ratio = 19;
    private Text _text;

    void Start()
    {
        _text = GetComponent<Text>();
    }

    void OnGUI()
    {
        if (_ratio == 0) return;
        float finalSize = Screen.height / _ratio;
        _text.fontSize = (int)finalSize;
    }
}

