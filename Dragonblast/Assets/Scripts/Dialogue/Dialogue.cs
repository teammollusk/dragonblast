﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/* Dialogue Script
 * requires: trigger
 * Start: 
 * Dialogue is parsed and stored in _dialogue[]
 * on trigger enter (player):
 * 	DialogueLabel is instantiated and placed above this
 * 	if _startDialogue button is pressed:
 * 		GameState is changed to Dialogue, DialogueCanvas is enabled, and text is populated with _dialogue[0]
 * 		if _advanceDialogue button is pressed:
 * 			if there is more dialogue:
 * 				more dialogue is displayed and goto previous instruction
 * 			else:
 * 				GameState is changed to Overworld and DialogueCanvas is destroyed
 * on trigger exit (player):
 * 	DialogueLabel is destroyed
 * 
 * note:
 * 	Dialogue should be in the format: name|text;name|text;name|text
 * 	where name is the name of the person speaking and text is what they say.
*/

public class Dialogue : GameFSMBehaviour
{
    [SerializeField]
	private string[] _dialogueSequence = { };
    [SerializeField]
    private GameObject _dialogueLabel = default;
    [SerializeField]
    private Directions _initialFacingDirection = Directions.None;

    private const float _dialogueSpeed = 0.01f;
    private const float _autoAdvanceWaitTime = _dialogueSpeed * 5;

    private int _dialogueIndex = 0;
    private string _startDialogue = Constants.SelectInput, _cancelDialogue = Constants.CancelInput;
	private char _nameDelimiter = '|', _textDelimiter = ';';
	private string[] _dialogue;
	private int _position = -1;
    private GameObject _dialogueCanvasInstance;
    private SpriteRenderer _dialogueLabelRenderer;
	private Text _textCanvas;
	private bool _hasTarget = false, _isOnCooldown = false;
	private Collider2D _target;
	private MovementController _movementController;
    private bool _writingText = false;
    private bool _dialogueOpen = false;

    public string DialogueText { get { return _dialogueSequence[_dialogueIndex]; } }
    public string[] DialogueSequence { get { return _dialogueSequence; } }
	
    void Awake()
    {
		_movementController = GetComponent<MovementController>();
	}

	void Start () 
	{
		_dialogue = _dialogueSequence[0].Split(_textDelimiter);

        var dialogueLabelInstance = Instantiate(_dialogueLabel);
        dialogueLabelInstance.transform.SetParent(this.transform, false);
        dialogueLabelInstance.transform.position = this.transform.position + _dialogueLabel.transform.position;
        dialogueLabelInstance.transform.rotation = Quaternion.identity;
        _dialogueLabelRenderer = dialogueLabelInstance.GetComponent<SpriteRenderer>();
    }

	public override void UpdateOverworld() 
	{
		if(_hasTarget)
		{
			if(Input.GetButtonDown(_startDialogue))
			{
				if(!_isOnCooldown)
				{
					StartDialogue();
				}
			}
		}
	}

	public override void UpdateDialogue() 
	{
        if (!_hasTarget && !_dialogueOpen)
            return;

        if (!_writingText && Input.GetButtonDown(_startDialogue))
		{
			AdvanceDialogue();
		}
        else if (_writingText && (Input.GetButtonDown(_startDialogue) || Input.GetButtonDown(_cancelDialogue)))
        {
            SkipTextifyDialogue();
        }
    }

	public void OnTriggerEnter2D(Collider2D other) 
	{
		if(other.tag == Constants.PlayerTag)
		{
            //render something to show dialogue prompt
            _dialogueLabelRenderer.enabled = true;
			_hasTarget = true;
			_target = other;
		}
	}

	public void OnTriggerExit2D(Collider2D other) 
	{
		if(other.tag == Constants.PlayerTag)
		{
            _dialogueLabelRenderer.enabled = false;
			_hasTarget = false;
            if(_initialFacingDirection != Directions.None)
            {
                _movementController.Face(_initialFacingDirection.ToString());
            }
		}
	}

	protected void StartDialogue()
	{
        _dialogueOpen = true;

        // Face towards player
        if (_movementController != null)
        {
            _movementController.Face(Utils.GetFacingDirection(transform.position, _target.transform.position));
        }
        

		TheGame.TriggerState(GameState.Dialogue);
		_dialogueCanvasInstance = TheObjectPool.Get(PooledObjects.DialogueCanvas);
		_dialogueCanvasInstance.SetActive(true);
        _textCanvas = _dialogueCanvasInstance.transform.Find("Container/DialogueText").GetComponent<Text>();
        AdvanceDialogue();
	}

	protected virtual void EndDialogue()
	{
        _dialogueOpen = false;

		_position = -1;
		_dialogueCanvasInstance.SetActive(false);
		TheGame.TriggerState(GameState.Overworld);
        _dialogueLabelRenderer.color = Color.grey;
        if(_dialogueIndex+1 < _dialogueSequence.Length)
        {
            UpdateDialogue(_dialogueSequence[++_dialogueIndex]);
        }

        var dialogueEvents = GetComponents<IDialogueEvent>();
        if(dialogueEvents != null)
        {
            foreach(var dialogueEvent in dialogueEvents)
            {
                dialogueEvent.Execute();
            }
        }
	}

    private void UpdateDialogue(string newDialogue)
    {
        _dialogue = newDialogue.Split(_textDelimiter);
        if(_dialogueLabelRenderer != null)
            _dialogueLabelRenderer.color = Color.white;
    }

    public void UpdateDialogueSequence(string[] newDialogueSequence, int newDialogueIndex = -1)
    {
        _dialogueSequence = newDialogueSequence;
        if(newDialogueIndex >= 0)
        {
            _dialogueIndex = newDialogueIndex;
        }
        UpdateDialogue(_dialogueSequence[_dialogueIndex]);
    }

	private void AdvanceDialogue()
	{
		if(++_position < _dialogue.Length)
		{
			StopCoroutine("TextifyCanvas");
			StartCoroutine("TextifyCanvas",_dialogue[_position]);
		}
		else
		{
			EndDialogue();
		}
	}

    private void SkipTextifyDialogue()
    {
        var dialogue = _dialogue[_position];
        if (dialogue[dialogue.Length-1] == Constants.DialogueAutoAdvance)
        {
            dialogue = dialogue.Substring(0, dialogue.Length - 1);
            StartCoroutine(AutoAdvanceDialogue());
        }
        _textCanvas.text = FormatDialogue(dialogue);
    }

    private IEnumerator AutoAdvanceDialogue()
    {
        yield return new WaitForSeconds(_autoAdvanceWaitTime);
        AdvanceDialogue();
    }

	private IEnumerator TextifyCanvas(string dialogueText)
	{
        _writingText = true;
        dialogueText = FormatDialogue(dialogueText);
		_textCanvas.text = "";
		while(_textCanvas.text.Length < dialogueText.Length)
		{
			if(_textCanvas != null) 
			{
                if(dialogueText[_textCanvas.text.Length] == Constants.DialogueAutoAdvance)
                {
                    yield return AutoAdvanceDialogue();
                }
                else
                {
                    _textCanvas.text += dialogueText[_textCanvas.text.Length];
                    yield return new WaitForSeconds(_dialogueSpeed);
                }
			}
			else 
			{
                Debug.LogError("Text canvas null for dialogue");
				break;
			} 
		}
        _writingText = false;
	}

    private string FormatDialogue(string unformattedDialogue)
    {
        string[] splitString = unformattedDialogue.Split(_nameDelimiter);
        if(splitString.Length > 1)
        {
            var name = splitString[0];
            var message = splitString[1];
            return string.Format("{0}:  {1}", name, message);
        }
        return unformattedDialogue;
    }

    // Called by SourcelessDialogue so it can run without a target
    protected void SetPhantomTarget()
    {
        _hasTarget = true;
    }

    public void Disable()
    {
        GetComponent<CircleCollider2D>().enabled = false;
        _dialogueLabelRenderer.enabled = false;
        _hasTarget = false;
    }
}
