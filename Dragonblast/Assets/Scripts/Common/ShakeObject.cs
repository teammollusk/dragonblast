﻿using UnityEngine;
using System.Collections;

public class ShakeObject : MonoBehaviour {

    [SerializeField]
    private float _shakeIntensity = 2;

    Vector3 _startingPosition;

	// Use this for initialization
	void Start () {
        _startingPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        transform.position += GetRandomTetheredDirection() * _shakeIntensity * Screen.width/1280;
	}

    public Vector3 GetRandomTetheredDirection()
    {
        Vector2 dir;
        float xMin = -1f;
        float xMax = 1f;
        float yMin = -1f;
        float yMax = 1f;

        // If you're to the right of your starting point, don't go further right, etc
        if (transform.position.x < _startingPosition.x)
        {
            xMin = 0;
        }
        if (transform.position.x > _startingPosition.x)
        {
            xMax = 0;
        }
        if (transform.position.y < _startingPosition.y)
        {
            yMin = 0;
        }
        if (transform.position.y > _startingPosition.y)
        {
            yMax = 0;
        }

        dir = new Vector2(Random.Range(xMin, xMax), Random.Range(yMin, yMax));

        return dir;
    }
}
