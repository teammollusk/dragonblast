﻿using UnityEngine;
using System.Collections;

public class PartyInitializer : MonoBehaviour 
{
	public AllyPartyMember[] PartyMembers;

	private void Start()
	{
		TheParty.Init(PartyMembers);
		Destroy(gameObject);
	}
}
