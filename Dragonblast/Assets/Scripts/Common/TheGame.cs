﻿using UnityEngine;
using UnityEditor;

public enum GameState
{
	Combat,
	Dialogue,
	Menu,
	Overworld,
	Transition
};

public class TheGame
{
	private static GameState _currentState = GameState.Overworld;

	public static GameState GetState()
	{
		return (_currentState);
	}

	public static void TriggerState(GameState newState)
	{
		var behaviors = Object.FindObjectsOfType<GameFSMBehaviour>();

		switch (_currentState)
		{
			case GameState.Combat:
				foreach (var b in behaviors)
					b.OnExitCombat();
				break;
			case GameState.Dialogue:
				foreach (var b in behaviors)
					b.OnExitDialogue();
				break;
			case GameState.Menu:
				foreach (var b in behaviors)
					b.OnExitMenu();
				break;
			case GameState.Overworld:
				foreach (var b in behaviors)
					b.OnExitOverworld();
				break;
			case GameState.Transition:
				foreach (var b in behaviors)
					b.OnExitTransition();
				break;
		}

		switch (newState)
		{
			case GameState.Combat:
				foreach (var b in behaviors)
					b.OnEnterCombat();
				break;
			case GameState.Dialogue:
				foreach (var b in behaviors)
					b.OnEnterDialogue();
				break;
			case GameState.Menu:
				foreach (var b in behaviors)
					b.OnEnterMenu();
				break;
			case GameState.Overworld:
				foreach (var b in behaviors)
					b.OnEnterOverworld();
				break;
			case GameState.Transition:
				foreach (var b in behaviors)
					b.OnEnterTransition();
				break;
		}

		_currentState = newState;
	}
}