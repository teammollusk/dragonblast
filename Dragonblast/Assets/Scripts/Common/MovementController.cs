using UnityEngine;
using System.Collections;

public class MovementController : GameFSMBehaviour
{
    private GameObject _rotator; // The child GameObject that will rotate to record which direction the character is facing
    private GameObject _weapon;
    private Rigidbody2D _rigidBody;
    private string _currentWalkingDir = Constants.None;
    private string _autoMoveFinalFacingDirection = Constants.Down;
    private Attacker _attacker;
    private Battler _battler;
    private MovementAI _AI;
    private bool _isPaused = false;
    private Vector3 _autoMoveDestination = Vector3.zero;
    private Animator _animator;

    public string FacingDirection { get; private set; }
    public bool IsAutoMoving { get; private set; }

    public float Speed { get; set; }


    public virtual void Awake()
	{
		Transform rotatorTransform = this.transform.Find(Constants.RotatorName);
        if ((_rotator = rotatorTransform.gameObject) == null)
        {
            Debug.LogWarning("No child GameObject named Rotator underneath GameObject with OWMovement attached: " + gameObject.name);
        }

		if((_animator = GetComponent<Animator>()) == null)
		{
			Debug.LogError("No Animator attached to GameObject with CharacterMovement: " + gameObject.name);
			Debug.Break();
		}
        _rigidBody = GetComponent<Rigidbody2D>();
        _attacker = GetComponent<Attacker>();
        _AI = GetComponent<MovementAI>();

        FacingDirection = Constants.Down;
        IsAutoMoving = false;
        Speed = Constants.PlayerMovementSpeed;
    }

    public void OnEnable()
    {
        _battler = GetComponent<Battler>();
    }

    public override void UpdateOverworld()
	{
        Move(ChooseDirection());
    }

	public override void UpdateCombat()
	{
        if (IsAutoMoving)
            Move(ChooseDirection());
        else
            Move(Vector2.zero);
    }

	public override void UpdateDialogue()
	{
        if (IsAutoMoving)
            Move(ChooseDirection());
        else
            Move(Vector2.zero);
    }

    public override void UpdateMenu()
    {
        Move(Vector2.zero);
    }

	public override void UpdateTransition()
	{
		Move(Vector2.zero);
	}

	public void Pause(bool pause)
    {
        _isPaused = pause;
    }

    public virtual Vector2 ChooseDirection()
    {
        if (_isPaused)
            return Vector2.zero;
        if (_AI)
            return _AI.ChooseDirection();
        if(IsAutoMoving)
        {
            var direction = _autoMoveDestination - transform.position;
            if (direction.magnitude < Constants.AutoMoveEpsilon)
            {
                Face(_autoMoveFinalFacingDirection);
                transform.position = _autoMoveDestination;
                IsAutoMoving = false;
            }
            return direction;
        }
        if(gameObject.tag.Equals(Constants.PlayerTag))
            return ChooseDirectionFromInput();

        return Vector2.zero;
    }

    private Vector2 ChooseDirectionFromInput()
    {
        // Get raw input
        float y = Input.GetAxisRaw("Vertical");
        float x = Input.GetAxisRaw("Horizontal");

        return new Vector2(x, y);
    }

    protected virtual void Move(Vector2 moveDir)
	{
        if((_attacker && _attacker.IsAttacking) || (_battler && _battler.IsDefending))
        {
            _rigidBody.velocity = Vector2.zero;
            return;
        }

        if(IsAutoMoving)
        {
            SimpleMove(moveDir);
            return;
        }

		//make cardinal movement have a larger range of values
		if(Mathf.Abs(moveDir.y) < Constants.CardinalMovementBias)
		{
			moveDir.y = 0f;
		}
		if(Mathf.Abs(moveDir.x) < Constants.CardinalMovementBias)
		{
			moveDir.x = 0f;
		}

		if(moveDir.x == 0f && moveDir.y == 0f)
		{
			Stand();
            if (_rigidBody)
            {
                _rigidBody.velocity = Vector2.zero;
            }
			return;
		}

		// Normalize speed (no moving faster or slower based on thumbstick position)
		if(moveDir.y != 0) 
		{
			moveDir.y /= Mathf.Abs(moveDir.y);
		}
		if(moveDir.x != 0) 
		{
			moveDir.x /= Mathf.Abs(moveDir.x);
		}

		// Normalize speed of diagonal movement
		if(moveDir.y != 0 && moveDir.x != 0)
		{
			moveDir.y /= Mathf.Sqrt(2);
			moveDir.x /= Mathf.Sqrt(2);
		}
		
		// Apply speed magnitude
		moveDir.y *= Speed;
		moveDir.x *= Speed;

        // Perform movement
        if (_rigidBody)
        {
            _rigidBody.velocity = moveDir;
        }
		
		// Animate sprite

		// If moving diagonally, decide movement animation based on previous cardinal movement animation
		// As long as you're still moving towards that cardinal direction
		if(moveDir.y != 0 && moveDir.x != 0 && StillMovingThatWay(moveDir))
		{
			Walk(_currentWalkingDir);
		}
		else
		{
            AnimateMovement(moveDir);
		}
	}

    protected virtual void SimpleMove(Vector2 moveDir)
    {
        _rigidBody.velocity = moveDir.normalized * Speed;

        if (!_attacker || !_attacker.IsAttacking)
        {
            AnimateMovement(moveDir);
        }
    }

    public void AutoMoveTo(Vector3 destination, string finalFacingDirection)
    {
        _autoMoveFinalFacingDirection = finalFacingDirection;
        AutoMoveTo(destination);
    }

    public void AutoMoveTo(Vector3 destination)
    {
        _autoMoveDestination = destination;
        IsAutoMoving = true;
    }

    public void CancelAutoMove()
    {
        IsAutoMoving = false;
    }

    protected virtual void AnimateMovement(Vector2 moveDir)
    {
        if(Mathf.Abs(moveDir.x) < .01f && Mathf.Abs(moveDir.y) < .01f)
        {
            Stand();
        }
        else
        {
            Walk(Utils.GetFacingDirection(Vector3.zero, moveDir));
        }
    }
	
	private void Walk(string dir)
	{
		_animator.Play(string.Format("{0}{1}", Constants.Walk, dir));
		_currentWalkingDir = dir;
		Face(dir);
	}
	
	private void Stand()
	{
		_animator.Play(string.Format("{0}{1}", Constants.Face, FacingDirection));
		_currentWalkingDir = Constants.None;
	}

    public void Face(string dir)
    {
        FaceSprite(dir);
        FaceColliders(dir);
    }

    private void FaceSprite(string dir)
    {
        FacingDirection = dir;
    }

    /// <summary>
    /// Rotate the colliders gameobject so weapon hitboxes will extend in the correct direction
    /// </summary>
    /// <param name="dir"></param>
    private void FaceColliders(string dir)
	{
        if (_rotator == null)
        {
            return;
        }

        float z = 0;
		switch(dir)
		{
		case Constants.Up:
			z = 0;
			break;
		case Constants.Down:
			z = 180;
			break;
		case Constants.Right:
			z = 270;
			break;
		case Constants.Left:
			z = 90;
			break;
		}

        _rotator.transform.localEulerAngles = new Vector3(0, 0, z);
	}

	private bool StillMovingThatWay(Vector2 moveDir)
	{
		switch(_currentWalkingDir)
		{
		case Constants.Up:
			if(moveDir.y > 0) return true;
			break;
		case Constants.Down:
			if(moveDir.y < 0) return true;
			break;
		case Constants.Right:
			if(moveDir.x > 0) return true;
			break;
		case Constants.Left:
			if(moveDir.x < 0) return true;
			break;
		}

		return false;
	}
}