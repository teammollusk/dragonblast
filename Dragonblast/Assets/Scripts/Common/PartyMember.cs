﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public abstract class PartyMember 
{
    public string Name;
	public int Health;
	public int Damage;
	public float Speed;
	public int StartingPosition;
    public GameObject TimelineIcon;
    public RuntimeAnimatorController AnimatorController;
    public GameObject Weapon;
    public string[] Skills;
    public float AttackDistance = 1;
}
