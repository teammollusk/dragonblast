using System;
using System.Collections.Generic;
using UnityEngine;

public enum Directions
{
    Up,
    Down,
    Left,
    Right,
    None
}

public class Constants
{
	// Directions
	public const string Up = "Up";
	public const string Down = "Down";
	public const string Right = "Right";
	public const string Left = "Left";
	public const string None = "None";

	// Animation prefixes (append Directions to them to get animation states)
	public const string Walk = "Walk";
	public const string Face = "Face";
    public const string Attack = "Attack";
    public const string Defend = "Defend";

    // Tags
    public const string PlayerTag = "Player";
    public const string PlayerWeaponTag = "PlayerWeapon";
    public const string PlayerSpriteTag = "PlayerSprite";
    public const string AllyBattlerTag = "AllyBattler";
    public const string EnemyBattlerTag = "EnemyBattler";

    // Weapons
    public const string StaffSprite = "StaffSprite";

    // Input names
    public const string AttackInput = "Fire1";
    public const string SelectInput = "Submit";
    public const string CancelInput = "Cancel";

    // Battle actions
    public static readonly string[] BattleActions = new string[] { "Attack", "Skills", "Defend" };
    public const string AttackAction = "Attack";
    public const string SkillsAction = "Skills";
    public const string DefendAction = "Defend";

    // Movement numbers
    public const float PlayerMovementSpeed = 5;
    public const float BattlerMovementSpeed = 10;
    public const float EnemyMovementSpeed = 2;
    public const float EnemyMovementSpeedAggroed = 6;
    public const float CardinalMovementBias = .35f;
    public const float AutoMoveEpsilon = 0.15f;

    // Sprite Layers
    public const string PlayerLayer = "Player";
    public const string WeaponBelowPlayerLayer = "WeaponBelowPlayer";
    public const string WeaponAbovePlayerLayer = "WeaponAbovePlayer";
    public const string BattlerBelowLayer = "BattlerBelow";
    public const string BattlerAboveLayer = "BattlerAbove";
    public const int TimelineIconLayer = 10;

    // Dialogue special chars
    public const char DialogueAutoAdvance = ']';

    // GameObject names
    public const string RotatorName = "Rotator";

    // LayerMasks
    public const int DefaultOnlyLayerMask = 1; // equates to "Default physics layer only" when cast as a LayerMask, so raycasts using this mask will ignore everything else

    // Skills
    public static readonly Dictionary<string, Skill> SkillsDictionary = new Dictionary<string, Skill>()
    {
        {"Yell", new YellSkill() },
        {"Suicide", new SuicideSkill() },
        {"Focus", new FocusSkill() },
        {"Shockwave", new ShockwaveSkill() },
        {"Win", new WinSkill() },
        {"Eat Burrito", new EatBurritoSkill() }
    };

	// Resource paths
	public const string PrefabPath = "Prefabs";

	// Battle
	public const int MaxBattlers = 6;
    public const int BattlePartySize = 3;
    public const int TimelinePositionBonus = 10; // Added to the starting timeline positions of allies or enemies, depending on who got the hit in the overworld
    public const float BattlerWidth = 1.0f;
    public const float PersonalBubbleDiameter = 1.3f;
    public const float AllyBattlerDistanceFromEnemy = 3.5f;

    // Misc
    public const float RaycastWidthDefault = 0.3f;
    public const float BaseDeltaTime = .01656f;
    public static readonly Vector3 MiddleOfNowhere = new Vector3(999, 999, 999);
}

