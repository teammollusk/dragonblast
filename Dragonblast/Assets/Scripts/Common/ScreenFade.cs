﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScreenFade : MonoBehaviour {

    private float _fadeStep;
    private float _fadeEndWait;
    private Image _image;

    public bool IsFading { get; set; }

    public static ScreenFade Create(Color? color = null, float fadeStep = .04f, float fadeEndWait = .1f)
    {
		GameObject screenFadeObject = TheObjectPool.Get(PooledObjects.ScreenFade);
        ScreenFade screenFade = screenFadeObject.GetComponent<ScreenFade>();

        screenFade._image = screenFadeObject.GetComponent<Image>();
        var fadeColor = color ?? Color.black;
        fadeColor.a = 0;
        screenFade._image.color = fadeColor;

        screenFade._fadeStep = fadeStep;
        screenFade._fadeEndWait = fadeEndWait;

		screenFadeObject.SetActive(true);

        return screenFade;
    }

    public void FadeIn()
    {
        StopAllCoroutines();
        StartCoroutine(FadeInRoutine());
    }

    public void FadeOut()
    {
        StopAllCoroutines();
        StartCoroutine(FadeOutRoutine());
    }

    private IEnumerator FadeInRoutine()
    {
        IsFading = true;
        var r = _image.color.r;
        var g = _image.color.g;
        var b = _image.color.b;
        while (_image.color.a < 1)
        {
            _image.color = new Color(r, g, b, _image.color.a + _fadeStep);
            yield return new WaitForSeconds(.01f);
        }
        yield return new WaitForSeconds(_fadeEndWait);
        IsFading = false;
    }

    private IEnumerator FadeOutRoutine()
    {
        IsFading = true;
        var r = _image.color.r;
        var g = _image.color.g;
        var b = _image.color.b;
        while (_image.color.a > 0)
        {
            _image.color = new Color(r, g, b, _image.color.a - _fadeStep);
            yield return new WaitForSeconds(.01f);
        }
        yield return new WaitForSeconds(_fadeEndWait);
        IsFading = false;

		gameObject.SetActive(false);
	}


}
