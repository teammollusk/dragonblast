﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Utils
{
    public static bool IsObstructed(Vector3 start, Vector3 end, LayerMask raycastLayerMask, float width = Constants.RaycastWidthDefault)
    {
        var raycastDirection = end - start;
        RaycastHit2D raycast = Physics2D.CircleCast(start, width, raycastDirection, raycastDirection.magnitude, raycastLayerMask);

        return raycast.collider != null;
    }

    public static string GetFacingDirection(Vector3 facerPosition, Vector3 targetPosition)
    {
        string dirString;
        Vector3 dirVector;

        GetFacingDirectionBase(facerPosition, targetPosition, out dirString, out dirVector);

        return dirString;
    }

    public static string GetFacingDirection(Vector3 facerPosition, Vector3 targetPosition, out Vector3 directionVector)
    {
        string dirString;
        Vector3 dirVector;

        GetFacingDirectionBase(facerPosition, targetPosition, out dirString, out dirVector);

        directionVector = dirVector;
        return dirString;
    }

    private static void GetFacingDirectionBase(Vector3 facerPosition, Vector3 targetPosition, out string dirString, out Vector3 dirVector)
    {
        // Use larger of x and y differential
        float yDiff = facerPosition.y - targetPosition.y;
        float xDiff = facerPosition.x - targetPosition.x;
        if (Mathf.Abs(yDiff) > Mathf.Abs(xDiff))
        {
            // if y differential is larger, return up/down
            if (yDiff > 0)
            {
                dirString = Constants.Down;
                dirVector = Vector3.down;
            }
            else
            {
                dirString = Constants.Up;
                dirVector = Vector3.up;
            }
        }
        else
        {
            // if x differential is larger, return left/right
            if (xDiff > 0)
            {
                dirString = Constants.Left;
                dirVector = Vector3.left;
            }
            else
            {
                dirString = Constants.Right;
                dirVector = Vector3.right;
            }
        }
    }

    public static Vector3 GetVectorFromDirectionString(string direction)
    {
        switch(direction)
        {
            case Constants.Up:
                return Vector2.up;
            case Constants.Down:
                return Vector2.down;
            case Constants.Left:
                return Vector2.left;
            case Constants.Right:
                return Vector2.right;
            default:
                return Vector2.zero;
        }
    }

    public static float GetAnimationClipLength(Animator animator, string clipName)
    {
        RuntimeAnimatorController rac = animator.runtimeAnimatorController;
        for(int i = 0; i < rac.animationClips.Length; i++)
        {
            if(rac.animationClips[i].name.EndsWith(clipName))
            {
                return rac.animationClips[i].length;
            }
        }
        return -1;
    }

    public static bool VectorsApproxEqual(Vector2 a, Vector2 b)
    {
        return FloatsApproxEqual(a.x, b.x) && FloatsApproxEqual(a.y, b.y);
    }

    public static bool FloatsApproxEqual(float a, float b)
    {
        return Math.Abs(a - b) < .001f;
    }


    public static void ShuffleList<T>(List<T> list)
    {
        for(int i = 0; i < list.Count; i++)
        {
            T temp = list[i];
            int randomIndex = UnityEngine.Random.Range(i, list.Count);
            list[i] = list[randomIndex];
            list[randomIndex] = temp;
        }
    }

    public static Vector3 FindFreeSpaceNearMe(Vector3 myPosition, string facingDirection)
    {
        var facingDirectionVector = Utils.GetVectorFromDirectionString(facingDirection) * Constants.PersonalBubbleDiameter;

        const int NumAdjSpaces = 4;
        Vector3[] adjacentSpaces = new Vector3[NumAdjSpaces];
        adjacentSpaces[0] = myPosition + new Vector3(facingDirectionVector.y, facingDirectionVector.x); // side
        adjacentSpaces[1] = myPosition - new Vector3(facingDirectionVector.y, facingDirectionVector.x); // other side
        adjacentSpaces[2] = myPosition - facingDirectionVector; // behind
        adjacentSpaces[3] = myPosition + facingDirectionVector; // in front

        for(int i = 0; i < NumAdjSpaces; i++)
        {
            if (!Utils.IsObstructed(myPosition, adjacentSpaces[i], Constants.DefaultOnlyLayerMask))
            {
                return adjacentSpaces[i];
            }
        }

        Debug.LogError(string.Format("No free space near {0} found. Consider making this area wider.", myPosition));
        return myPosition;
    }

    public static bool WorldPointIsVisible(Vector3 point)
    {
        // Camera.main should always exist, but this often throws an exception when I quit debugging, 
        // Presumably because the camera is destroyed before Enemy::OnBecameInvisible is called,
        // So null checking
        if (Camera.main)
        {
            var viewportPoint = Camera.main.WorldToViewportPoint(point);
            return viewportPoint.x > -.1 && viewportPoint.x < 1.1 && viewportPoint.y > -0.1 && viewportPoint.y < 1.1;
        }

        return false;
    }
}
