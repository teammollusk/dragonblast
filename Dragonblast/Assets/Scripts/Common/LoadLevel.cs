﻿using UnityEngine;
using UnityEngine.SceneManagement;

//TODO use sprite renderer for rendering the title screen game objects so that a free flowing camera can be used.
//make sure all buttons work

//TODO NOT EVER USING A MOUSE EVER ONLY KEYBOARD AND GAMEPAD
//Figure out how to navigate buttons with a keyboard
    //Still use normal buttons but manipulate the onclick event to handle pressing "Enter" (InputManager may have to be utilized as well)
    //Use a selector, control selector with arrow keys (up and down)



//TODO this script needs to pan the camera to the first level and any save file level
//start from the clouds then the camera "scrolls" down to justin's house
//use coroutines to get the camera to pan
//trees, birds, hearing kids laugh while panning adds to the setting
//needs to be controlled with keyboard and gamepad
public class LoadLevel : MonoBehaviour
{
    //this will load the save file's level (scene)
    public void Load(string levelToLoad)
    {
        Debug.Log("About to load");
        SceneManager.LoadScene(levelToLoad);
    }

    //need handling for keyboard and gamepad, only works with clicks right now
    public void Close()
    {
        Debug.Log("About to close");
        Application.Quit();
        /* if(UNITY_EDITOR == true)
         {
             UnityEditor.EditorApplication.isPlaying = false;
         }*/

#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif

        Debug.Log("Should have closed already");
    }
}
