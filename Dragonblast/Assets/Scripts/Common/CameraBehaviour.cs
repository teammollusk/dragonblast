﻿using UnityEngine;
using System.Collections;

public class CameraBehaviour : GameFSMBehaviour 
{
	public GameObject Character;

	public void Start () 
	{
		if(Character == null)
		{
			Debug.Log("No character selected for the camera to follow!");
		}
	}

	public override void UpdateOverworld() 
	{
		this.transform.position = new Vector3 (Character.transform.position.x, Character.transform.position.y, this.transform.position.z);
	}
}
