﻿using UnityEngine;
using System.Collections;

public class GameFSMBehaviour : MonoBehaviour 
{
	// Update is called once per frame
	public void Update() 
	{
		switch(TheGame.GetState())
		{
			case GameState.Combat:
				UpdateCombat();
				break;
			case GameState.Dialogue:
				UpdateDialogue();
				break;
			case GameState.Menu:
				UpdateMenu();
				break;
			case GameState.Overworld:
				UpdateOverworld();
				break;
			case GameState.Transition:
				UpdateTransition();
				break;
		}
	}

	public virtual void OnEnterCombat() { }
	public virtual void OnExitCombat() { }
	public virtual void UpdateCombat() { }

	public virtual void OnEnterDialogue() { }
	public virtual void OnExitDialogue() { }
	public virtual void UpdateDialogue() { }

	public virtual void OnEnterMenu() { }
	public virtual void OnExitMenu() { }
	public virtual void UpdateMenu() { }

	public virtual void OnEnterOverworld() { }
	public virtual void OnExitOverworld() { }
	public virtual void UpdateOverworld() { }

	public virtual void OnEnterTransition() { }
	public virtual void OnExitTransition() { }
	public virtual void UpdateTransition() { }
}