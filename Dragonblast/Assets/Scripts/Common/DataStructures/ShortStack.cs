﻿/* doublyLinkedList implementation of a stack that has a max size.
 * If capacity is reached and an element is added then the last element is dropped.
 * @author Christian Casadio
 */ 
#pragma warning disable 0693
public class ShortStack<T>
{
	private Node<T> _Head;
	private Node<T> _Tail;
	private int _Capacity;
	private int _Size;

	public ShortStack()
	{
		_Size = 0;
		_Capacity = 10;
	}

	public ShortStack(int capacity)
	{
		_Size = 0;
		_Capacity = capacity;
	}

	public void Push(T element)
	{
		Node<T> temp = new Node<T>(element);
		temp.SetNext(_Head);
        _Head = temp;
        _Head.SetPrevious(temp);
		if (_Size == 0) 
		{
			_Tail = temp;
		}
		else if(_Size == _Capacity)
		{
			_Tail.GetPrevious().SetNext(null);
			_Tail = _Tail.GetPrevious();
			_Size--;
		}
		_Size++;
	}

	public T Pop()
	{
		T data = _Head.GetData();
		_Head.GetNext().SetPrevious(null);
		_Head = _Head.GetNext();
		_Size--;
		return data;
	}

	private class Node<T>
	{
		private T _Data;
		private Node<T> _Next;
		private Node<T> _Previous;

		public Node(T data)
		{
			_Data = data;
		}

		public T GetData()
		{
			return _Data;
		}

		public Node<T> GetNext()
		{
			return _Next;
		}

		public void SetNext(Node<T> next)
		{
			_Next = next;
		}

		public Node<T> GetPrevious()
		{
			return _Previous;
		}
		
		public void SetPrevious(Node<T> previous)
		{
			_Previous = previous;
		}
	}
}