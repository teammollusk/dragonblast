﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShortQueue<T> : Queue<T>
{
    int _maxLength;

    public ShortQueue(int maxLength)
    {
        _maxLength = maxLength;
    }

    public new void Enqueue(T item)
    {
        base.Enqueue(item);
        if(Count > _maxLength)
        {
            Dequeue();
        }
    }
}