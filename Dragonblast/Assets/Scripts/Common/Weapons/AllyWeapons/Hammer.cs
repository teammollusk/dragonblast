﻿using UnityEngine;
using System.Collections;
using System;

public class Hammer : Weapon
{
    [SerializeField]
    private Sprite _topSprite = default;
    [SerializeField]
    private Sprite _frontSprite = default;

    //private SpriteRenderer SpriteRenderer;
    private float _shockwaveSpeed = 6f;

    private const float ShockwaveStartOffset = .5f;
    private const float Cooldown = 1.2f;


    public override float GetCooldown() { return Cooldown; }
    public override void Wield() { /* Handled by Animation Events */ }


    #region Animation Events
    #region Up
    public override void Up1()
    {
        SpriteRenderer.sprite = _topSprite;
        OrderWeaponInFront();
        PositionWeapon(-.45f, -0.05f);
    }

    public override void Up2()
    {
        SpriteRenderer.sprite = _frontSprite;
        OrderWeaponBehind();
        PositionWeapon(-.3f, .1f, 180);
    }

    public override void Up3()
    {
        SpriteRenderer.sprite = _topSprite;
        PositionWeapon(-.3f, .25f, 180);
        FireShockwave();
    }
    #endregion

    #region Down
    public override void Down1()
    {
        SpriteRenderer.sprite = _topSprite;
        OrderWeaponBehind();
        PositionWeapon(.4f, -0.05f);
    }

    public override void Down2()
    {
        SpriteRenderer.sprite = _frontSprite;
        OrderWeaponInFront();
        PositionWeapon(.3f, .1f);
    }

    public override void Down3()
    {
        SpriteRenderer.sprite = _topSprite;
        PositionWeapon(.3f, .35f);
        FireShockwave();
    }
    #endregion

    #region Left
    public override void Left1()
    {
        OrderWeaponBehind();
        PositionWeapon(-.23f, -0.11f, 180);
    }
    public override void Left2()
    {
        PositionWeapon(.25f, 0.03f, 90);
    }
    public override void Left3()
    {
        PositionWeapon(.26f, .31f, 90);
        FireShockwave();
    }
    #endregion

    #region Right
    public override void Right1()
    {
        PositionWeapon(.23f, -0.11f, 180);
    }
    public override void Right2()
    {
        PositionWeapon(-.25f, .03f, -90);
    }
    public override void Right3()
    {
        PositionWeapon(-.26f, .31f, -90);
        FireShockwave();
    }
    #endregion

    public void FireShockwave()
    {
        if (TheGame.GetState() != GameState.Overworld)
        {
            // Don't fire a shockwave in combat because it clashes with ShockwaveSkill
            return;
        }

        var rotator = transform.parent;
        var startPosition = SpriteRenderer.transform.position + rotator.up * ShockwaveStartOffset;

        // If attacking to the left or right, adjust the shockwave to come from where the hammer touches the ground (a little lower)
        const float VerticalOffset = -.23f;
        if(Utils.FloatsApproxEqual(rotator.up.y, 0))
        {
            startPosition += new Vector3(0, VerticalOffset);
        }

		GameObject shockwave = TheObjectPool.Get(PooledObjects.Shockwave);
		shockwave.transform.SetPositionAndRotation(startPosition, rotator.rotation);
		shockwave.SetActive(true);
        shockwave.GetComponent<Rigidbody2D>().velocity = rotator.up * _shockwaveSpeed;
        shockwave.GetComponent<Shockwave>().Hammer = this;
    }
    #endregion
}
