﻿using UnityEngine;
using System.Collections;
using System;

public class Arrow : Weapon 
{
	private int _maxLifeSpan = 120;

	void Start()
	{
		StartCoroutine(Expire());
	}

	protected override void OnCollisionEnter2D(Collision2D c)
	{
        base.OnCollisionEnter2D(c);
		GetComponent<Rigidbody2D>().velocity = Vector2.zero;
	}

	void OnBecameInvisible()
	{
		Destroy(gameObject);
	}

	IEnumerator Expire()
	{
		yield return new WaitForSeconds(_maxLifeSpan);
		Destroy(gameObject);
	}

    public override void Wield()
    {
        throw new NotImplementedException();
    }

    public override float GetCooldown()
    {
        throw new NotImplementedException();
    }
}
