﻿using UnityEngine;
using System.Collections;

public abstract class Weapon : GameFSMBehaviour 
{
    private bool _hasCollided = false;

    protected SpriteRenderer SpriteRenderer { get; set; }


    public abstract float GetCooldown();
    public abstract void Wield();


    public void Awake()
    {
        SpriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        Wield();
    }

    public override void OnEnterOverworld()
    {
        _hasCollided = false;
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (_hasCollided) return;
        _hasCollided = true;

        TriggerCombat(collider);
    }

    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        TriggerCombat(collision.collider);
    }


    protected void TriggerCombat(Collider2D collider)
    {
        var combatInit = collider.GetComponent<CombatInitiation>();
        if (combatInit && combatInit.enabled)
        {
            combatInit.Execute(gameObject);
            Destroy();
        }
    }

    protected void PositionWeapon(float xOffset, float yOffset, float zRotation = 0)
    {
        transform.Rotate(Vector3.forward, zRotation);
        transform.position = transform.parent.position - new Vector3(xOffset, yOffset);
    }

    protected void OrderWeaponInFront()
    {
        if (SpriteRenderer)
        {
            SpriteRenderer.sortingLayerName = Constants.WeaponAbovePlayerLayer;
        }
    }

    protected void OrderWeaponBehind()
    {
        if (SpriteRenderer)
        {
            SpriteRenderer.sortingLayerName = Constants.WeaponBelowPlayerLayer;
        }
    }

    public virtual void Destroy()
    {
        Destroy(gameObject);
    }

    // Can be overridden, or not
    public virtual void Up1() { }
    public virtual void Up2() { }
    public virtual void Up3() { }
    public virtual void Up4() { }

    public virtual void Down1() { }
    public virtual void Down2() { }
    public virtual void Down3() { }
    public virtual void Down4() { }

    public virtual void Left1() { }
    public virtual void Left2() { }
    public virtual void Left3() { }
    public virtual void Left4() { }

    public virtual void Right1() { }
    public virtual void Right2() { }
    public virtual void Right3() { }
    public virtual void Right4() { }

    public virtual void End() { Destroy(); }
}
