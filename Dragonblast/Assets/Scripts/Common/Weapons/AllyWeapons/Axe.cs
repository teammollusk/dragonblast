﻿using UnityEngine;
using System.Collections;

public class Axe : Weapon 
{
    private MovementController _movementController;

    private const float SlideSpeed = 1.7f;
    private const float Cooldown = 0.7f;


    public override float GetCooldown() { return Cooldown; }

    void Start()
    {
        _movementController = transform.root.GetComponent<MovementController>();
        Wield();
    }

	public override void Wield()
	{
        // TODO: This doesn't actually work currently because movement is turned off while attacking by default
        // Need to decide whether to add an override or make turning off movement explicit per weapon
        if (_movementController != null)
        {
            _movementController.Speed *= SlideSpeed;
            var facingDir = Utils.GetVectorFromDirectionString(_movementController.FacingDirection);
            // Not worring about how far forward we are auto moving because End() will cancel it
            var arbitrarilyFarForwardDestination = facingDir * 10;
            _movementController.AutoMoveTo(transform.position + arbitrarilyFarForwardDestination);
        }
    }

    public override void End()
    {
        if(_movementController != null)
        {
            _movementController.Speed = Constants.PlayerMovementSpeed;
            _movementController.CancelAutoMove();
        }
        Destroy();
    }
}
