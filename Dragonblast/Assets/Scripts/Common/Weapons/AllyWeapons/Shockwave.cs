﻿using UnityEngine;
using System.Collections;
using System;

public class Shockwave : Weapon
{
    private const int MaxDistance = 1;
    private Vector3 _startPosition;
    public Hammer Hammer { get; set; }

	private void Start()
	{
		// No-op to prevent the parent Start from running and calling the non implemented Wield... =\
	}

	private void OnEnable()
    {
        _startPosition = transform.position;
        StartCoroutine(Expire());
    }

    IEnumerator Expire()
    {
        var distance = (transform.position - _startPosition).magnitude;
        var maxDistance = TheGame.GetState() == GameState.Combat ? MaxDistance * 1.5f : MaxDistance; // Increase distance while using Shockwave skill
        while(distance < maxDistance)
        {
            yield return new WaitForEndOfFrame();
            distance = (transform.position - _startPosition).magnitude;
        }
		gameObject.SetActive(false);
    }

    public override void Destroy()
    {
        Hammer.Destroy();
		gameObject.SetActive(false);
    }

    public override void Wield()
    {
        throw new NotImplementedException();
    }

    public override float GetCooldown()
    {
        throw new NotImplementedException();
    }
}
