﻿using UnityEngine;
using System.Collections;

public class Bow : Weapon 
{
	private float _cooldown = 2f;
	private float _arrowSpeed = 20f;
	private float _bowExistenceDuration = .5f;
	public GameObject _arrow;


    public override void Wield()
	{
		StartCoroutine(Shoot());
	}
	
	private IEnumerator Shoot()
	{
		GameObject arrow = (GameObject) Instantiate(_arrow, transform.position + ((_arrow.transform.localScale.y * .5f) * transform.up), transform.rotation);
		arrow.GetComponent<Rigidbody2D>().velocity = transform.up * _arrowSpeed;
		yield return new WaitForSeconds(_bowExistenceDuration);
		Destroy(gameObject);
	}
	
	public override float GetCooldown() { return _cooldown; }
}
