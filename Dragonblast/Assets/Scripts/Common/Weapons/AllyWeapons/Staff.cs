﻿using UnityEngine;
using System.Collections;
using System;

public class Staff : Weapon 
{
    private const float Cooldown = 0.7f;


    public override float GetCooldown() { return Cooldown; }

    public override void Wield() { /* Handled by Animation Events */ }

    #region Animation Events
    #region Up
    public override void Up1()
    {
        PositionWeapon(0.4f, 0.15f);
        OrderWeaponBehind();
    }

    public override void Up2()
    {
        PositionWeapon(-0.19f, -0.65f, -90);
    }

    public override void Up3()
    {
        PositionWeapon(-0.85f, 0.2f, -90);
        OrderWeaponInFront();
    }

    public override void Up4()
    {
        PositionWeapon(-0.85f, 0.1f);
    }
    #endregion

    #region Down
    public override void Down1()
    {
        PositionWeapon(-0.4f, 0.25f);
    }

    public override void Down2()
    {
        PositionWeapon(0.19f, 0.65f, -90);
    }

    public override void Down3()
    {
        PositionWeapon(0.85f, 0.15f, -90);
    }

    public override void Down4()
    {
        PositionWeapon(0.85f, 0.05f);
    }
    #endregion

    #region Left
    public override void Left1()
    {
        PositionWeapon(0.25f, 0.5f);
    }

    public override void Left2()
    {
        PositionWeapon(0.6f, 0.15f, -90);
    }

    public override void Left3()
    {
        PositionWeapon(0.0f, -0.55f, -90);
        OrderWeaponBehind();
    }

    public override void Left4()
    {
        PositionWeapon(-0.05f, -0.55f);
    }
    #endregion

    #region Right
    public override void Right1()
    {
        PositionWeapon(-0.3f, -0.2f);
        OrderWeaponBehind();
    }

    public override void Right2()
    {
        PositionWeapon(-0.6f, 0.2f, -90);
        OrderWeaponInFront();
    }

    public override void Right3()
    {
        PositionWeapon(0f, 0.55f, -90);
    }

    public override void Right4()
    {
        PositionWeapon(0.05f, 0.55f);
    }
    #endregion
    #endregion

}
