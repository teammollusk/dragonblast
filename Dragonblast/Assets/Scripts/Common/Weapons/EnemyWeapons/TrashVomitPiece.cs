﻿using UnityEngine;
using System.Collections;

public class TrashVomitPiece : MonoBehaviour
{
    private const int MaxDistance = 2;
    private Vector3 _startPosition;

    void Start()
    {
        _startPosition = transform.position;
        StartCoroutine(Expire());
    }

    IEnumerator Expire()
    {
        var distance = (transform.position - _startPosition).magnitude;
        while (distance < MaxDistance)
        {
            yield return new WaitForEndOfFrame();
            distance = (transform.position - _startPosition).magnitude;
        }
        Destroy(gameObject);
    }
}