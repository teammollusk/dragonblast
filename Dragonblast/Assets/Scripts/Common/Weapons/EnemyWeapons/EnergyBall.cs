﻿using UnityEngine;
using System.Collections;

public class EnergyBall : MonoBehaviour {

    protected void OnTriggerEnter2D(Collider2D c)
    {
        if(c.tag == Constants.AllyBattlerTag)
            Destroy(gameObject);
    }

    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
