﻿using UnityEngine;
using System.Collections;

public class EnergyAura : Weapon 
{
	private float _cooldown = 2f;
	private float _ballSpeed = 10f;
	private float _auraExistenceDuration = .8f;
	public GameObject _energyBall;
	
	public override void Wield()
	{
		StartCoroutine(Shoot());
	}
	
	private IEnumerator Shoot()
	{
        yield return new WaitForSeconds(_auraExistenceDuration);
        GameObject ball = (GameObject) Instantiate(_energyBall, transform.position + ((_energyBall.transform.localScale.y * .5f) * transform.up), _energyBall.transform.rotation);
		ball.GetComponent<Rigidbody2D>().velocity = transform.up * _ballSpeed;
		yield return new WaitForSeconds(_auraExistenceDuration);
		Destroy(gameObject);
	}
	
	public override float GetCooldown() { return _cooldown; }
}
