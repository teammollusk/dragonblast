﻿using System.Collections;
using UnityEngine;

public class TrashVomit : Weapon
{
    [SerializeField]
    private GameObject _trashVomitPiece = default;

    public override float GetCooldown() { return 0; }

    public override void Wield()
    {
        StartCoroutine(VomitTrash());
    }
    
    
    private IEnumerator VomitTrash()
    {
        const int NumTrashPieces = 10;
        const float VomitSpeed = 10f;
        var facingDirection = transform.parent.parent.GetComponent<MovementController>().FacingDirection;
        for(int i = 0; i < NumTrashPieces; i++)
        {
            // Randomize angle of this piece of trash
            var vomitVector = Utils.GetVectorFromDirectionString(facingDirection);
            var randomNumber = Random.Range(0f, .3f);
            if (Random.Range(0, 2) == 0) { randomNumber *= -1; }
            if (vomitVector.y == 0) { vomitVector.y = randomNumber; }
            else { vomitVector.x = randomNumber; }
            vomitVector.Normalize();
            vomitVector *= VomitSpeed;

            var vomitPiece = Instantiate(_trashVomitPiece, transform.position, transform.rotation) as GameObject;

            var vomitRigidBody = vomitPiece.GetComponent<Rigidbody2D>();
            vomitRigidBody.velocity = vomitVector;
            yield return new WaitForSeconds(.05f);
        }
    }
}