using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Singleton class for storing THE PARTY!!!
/// Intantiated by a PartyManager gameobject for ease of creating party members in the inspector
/// </summary>
public class TheParty
{
	private static List<AllyPartyMember> _partyMembers;
	// The animator component of the gameobject that has the party leader's sprite attached to it
	private static Animator _partyLeaderAnimator;
	
    public int Size { get { return _partyMembers.Count; } }

	public static void Init(AllyPartyMember[] partyMemberList)
	{
		_partyMembers = new List<AllyPartyMember>(partyMemberList);
		_partyLeaderAnimator = GameObject.FindWithTag(Constants.PlayerTag).GetComponent<Animator>();
	}
	
    public static AllyPartyMember GetMember(PartyMemberName name)
    {
        return GetMember(name.ToString());
    }

	public static AllyPartyMember GetMember(string name)
	{
		foreach(AllyPartyMember member in _partyMembers)
        {
            if(member.Name.Equals(name))
            {
                return member;
            }
        }
        return null;
	}
	
	public static void AddMember(AllyPartyMember partyMember)
	{
		_partyMembers.Add(partyMember);
	}
	
	public static AllyPartyMember GetLeader()
	{
		return _partyMembers[0];
	}

	public static void SetLeader(AllyPartyMember partyLeader)
	{
        // TODO: Figure out why 1% of the time an error is thrown here when the battle ends saying that the object of type Animator has been destroyed but I am still trying to access it
        if (_partyLeaderAnimator == null)
        {
            Debug.LogError("Couldn't switch party leader: MissingReferenceException: The object of type 'Animator' has been destroyed but you are still trying to access it. Your script should either check if it is null or you should not destroy the object.");
        }
        else
        {
            _partyMembers.Remove(partyLeader);
            _partyMembers.Insert(0, partyLeader);
            _partyLeaderAnimator.runtimeAnimatorController = partyLeader.AnimatorController;
        }
	}
	
	public static List<AllyPartyMember> GetMembers()
	{
		return _partyMembers;
	}
}

public enum PartyMemberName
{
    Justin,
    Scott,
    Brett
}