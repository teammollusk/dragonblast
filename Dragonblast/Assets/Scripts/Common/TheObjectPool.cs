﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class PooledObjects
{
	public const string Battler = "Battler";
	public const string DefendIcon = "DefendIcon";
	public const string Shockwave = "Shockwave";
	public const string FloatingCombatText = "FloatingCombatText";
	public const string Timeline = "Timeline";
	public const string ScreenFade = "ScreenFade";
	public const string DialogueCanvas = "DialogueCanvas";
	public const string SourcelessDialogue = "SourcelessDialogue";
	public const string ActivateEnemyDialogueEvent = "ActivateEnemyDialogueEvent";
	public const string PartyMember = "PartyMember";
}

public class TheObjectPool : MonoBehaviour
{
	private enum Category
	{
		Battle,
		Dialogue,
		Overworld,
		Common
	}


	private struct PooledObject
	{
		public Category category;
		public GameObject prefab;
		public int amount;

		public PooledObject(Category category, string prefabName, int amount)
		{
			this.category = category;
			this.prefab = (GameObject)Resources.Load(
				Path.Combine(Path.Combine(Constants.PrefabPath, category.ToString()), prefabName));
			this.amount = amount;
		}

		public PooledObject(Category category, string subFolder, string prefabName, int amount)
			: this(category, Path.Combine(subFolder, prefabName), amount)
		{
		}
	}

	private static Dictionary<string, GameObject[]> _pooledObjects = new Dictionary<string, GameObject[]>();


	private void Start()
	{
		// Add new objects to pool here
		PooledObject[] objectsToPool = new PooledObject[]
		{
			new PooledObject(Category.Battle, PooledObjects.Battler, Constants.MaxBattlers),
			new PooledObject(Category.Battle, PooledObjects.DefendIcon, Constants.MaxBattlers),
			new PooledObject(Category.Battle, PooledObjects.FloatingCombatText, Constants.MaxBattlers),
			new PooledObject(Category.Battle, PooledObjects.Timeline, 1),

			new PooledObject(Category.Dialogue, PooledObjects.DialogueCanvas, 1),
			new PooledObject(Category.Dialogue, PooledObjects.SourcelessDialogue, 1),
			new PooledObject(Category.Dialogue, PooledObjects.ActivateEnemyDialogueEvent, 1),

			new PooledObject(Category.Overworld, "Weapons", PooledObjects.Shockwave, Constants.MaxBattlers),
			new PooledObject(Category.Overworld, PooledObjects.PartyMember, 1),

			new PooledObject(Category.Common, PooledObjects.ScreenFade, 1)
		};

		// Create pools from the list and insert them into the map
		for (int i = 0; i < objectsToPool.Length; i++)
		{
			// In the first two steps we're just creating objects to child our pools to, to neatly organize them

			// 1. Find or create an empty gameobject with the name of the chosen category, and child it to this gameobject
			Transform parentTransform = gameObject.transform.Find(objectsToPool[i].category.ToString());
			if (parentTransform == null)
			{
				GameObject parent = new GameObject(objectsToPool[i].category.ToString());
				parent.transform.SetParent(gameObject.transform);
				parentTransform = parent.transform;
			}

			// 2. Create a subfolder specifically for this prefab, and child it to category gameobject
			string key = objectsToPool[i].prefab.name;
			GameObject subfolder = new GameObject(key);
			subfolder.transform.SetParent(parentTransform);

			// The next two steps are the meat

			// 3. Instantiate all the pooled objects and child them to the subfolder
			GameObject[] value = new GameObject[objectsToPool[i].amount];
			for (int j = 0; j < objectsToPool[i].amount; j++)
			{
				GameObject obj = (GameObject)Instantiate(objectsToPool[i].prefab);
				obj.transform.SetParent(subfolder.transform);
				obj.SetActive(false);
				value[j] = obj;
			}

			// 4. Map the name of the prefab to the array of instantiated copies of it
			_pooledObjects.Add(key, value);
		}
	}

	public static GameObject Get(string name)
	{
		if (!_pooledObjects.ContainsKey(name))
			return null;

		GameObject[] pool = _pooledObjects[name];
		for (int i = 0; i < pool.Length; i++)
		{
			if (!pool[i].activeInHierarchy)
				return pool[i];
		}

		return null;
	}
}
