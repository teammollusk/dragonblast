﻿using UnityEngine;

public class Ball : CombatInitiation
{
    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    public override void Execute(GameObject initiator)
    {
        if (_executed) return;
        _executed = true;

        var playStickballScript = transform.parent.GetComponent<PlayStickball>();

        if (initiator.tag == Constants.PlayerWeaponTag)
        {
            playStickballScript.RegisterWeaponHit();
            GoFlying();
        }
        else if (initiator.tag == Constants.PlayerTag)
        {
            playStickballScript.RegisterPlayerHit();
            Destroy(gameObject);
        }
    }

    private void GoFlying()
    {
        var movementController = GetComponent<MovementController>();
        movementController.enabled = false;

        var collider = GetComponent<Collider2D>();
        collider.enabled = false;

        
        var xVelocity = 20;
        // Randomly shoot in the range between 4 above and 9 above Scott   
        var yVelocity = Random.Range(4, 9);
        // Shoot below Scott half the time
        if(Random.Range(0, 2) == 0)
        {
            yVelocity *= -1;
        }

        var rigidbody = GetComponent<Rigidbody2D>();
        rigidbody.velocity = new Vector2(xVelocity, yVelocity);
    }
}
