﻿using UnityEngine;
using System.Collections;

public class Gate : MonoBehaviour {

	public void Open()
    {
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<BoxCollider2D>().enabled = false;
    }
}
