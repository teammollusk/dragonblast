﻿using UnityEngine;
using System.Collections;

public class PickupStick : PickupObject {

    [SerializeField]
    private Dialogue _npcDialogue = default;

    public override void Execute()
    {
        _player.GetComponent<Attacker>().enabled = true;
        UpdateNpcDialogue();
        base.Execute();
    }

    private void UpdateNpcDialogue()
    {
        string[] _currentDialogueSequence = _npcDialogue.DialogueSequence;
        for(int i = 0; i < _currentDialogueSequence.Length; i++)
        {
            _currentDialogueSequence[i] = _currentDialogueSequence[i].Replace("Go grab the stick.", "Go stand over there.");
        }
        _npcDialogue.UpdateDialogueSequence(_currentDialogueSequence);
    }

}
