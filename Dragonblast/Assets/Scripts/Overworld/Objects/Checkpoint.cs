﻿using UnityEngine;
using System.Collections;

public class Checkpoint : MonoBehaviour
{
    public void OnTriggerEnter2D(Collider2D c)
    {
        if (c.tag == Constants.PlayerTag)
        {
            var player = c.GetComponent<Player>();
            player.LastCheckpoint = c.transform.position;
        }
    }
}