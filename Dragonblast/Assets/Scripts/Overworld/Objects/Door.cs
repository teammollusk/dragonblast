﻿using UnityEngine;
using System.Collections;

public class Door : GameFSMBehaviour
{
    private bool _playerByDoor = false;
    private Transform _playerTransform;

    [SerializeField]
    private Transform _exitLocation = default;
    [SerializeField]
    private DoorDirection _exitDirection = default;

	void OnTriggerEnter2D(Collider2D c)
    {
        if(c.tag.Equals(Constants.PlayerTag))
        {
            _playerByDoor = true;
            _playerTransform = c.transform;
        }
    }

    void OnTriggerExit2D(Collider2D c)
    {
        if (c.tag.Equals(Constants.PlayerTag))
        {
            _playerByDoor = false;
        }
    }

    public override void UpdateOverworld()
    {
        var verticalInput = Input.GetAxis("Vertical");
        if(_playerByDoor
            && ((verticalInput > 0 && _exitDirection == DoorDirection.Up)
            || (verticalInput < 0 && _exitDirection == DoorDirection.Down))
            )
        {
            StartCoroutine(EnterDoor());
        }
    }

    private IEnumerator EnterDoor()
    {
		TheGame.TriggerState(GameState.Transition);

        _playerByDoor = false;
        var screenFade = ScreenFade.Create();
        screenFade.FadeIn();
        yield return new WaitUntil(() => !screenFade.IsFading);

        _playerTransform.position = _exitLocation.position;
        screenFade.FadeOut();
        yield return new WaitUntil(() => !screenFade.IsFading);

		TheGame.TriggerState(GameState.Overworld);
	}

	private enum DoorDirection
    {
        Up,
        Down
    }
}
