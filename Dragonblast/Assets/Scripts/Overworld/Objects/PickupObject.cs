﻿using UnityEngine;
using System.Collections;

public class PickupObject : MonoBehaviour, IDialogueEvent {

    [SerializeField]
    private GameObject _objectToPickup; // WIll get placed in inventory or something once that's implemented

    protected GameObject _player;

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == Constants.PlayerTag)
        {
            _player = other.gameObject;
        }

    }

    public virtual void Execute()
    {
        Destroy(this.gameObject);
    }
}
