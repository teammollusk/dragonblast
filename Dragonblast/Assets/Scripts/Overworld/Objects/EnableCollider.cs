﻿using UnityEngine;
using System.Collections;

public class EnableCollider : MonoBehaviour
{
    [SerializeField]
    private Collider2D _collider = default;
    [SerializeField]
    private bool _disableInstead = default;
    [SerializeField]
    private bool _executeOnceOnly = default;

    private bool _done = false;

    public void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.tag == Constants.PlayerTag && !_done)
        {
            _collider.enabled = !_disableInstead;
            if(_executeOnceOnly)
            {
                _done = true;
            }
        }
    }
}