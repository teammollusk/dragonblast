﻿using UnityEngine;
using System.Collections;

public class Attacker : GameFSMBehaviour
{
    MovementController _movementController;
    bool _isAttackReady = true;
    Animator _animator;
    Weapon _weapon;
    Transform _rotator;

    public bool IsAttacking { get; set; }

    public void Start()
    {
        _animator = GetComponent<Animator>();
        _movementController = GetComponent<MovementController>();
        _rotator = transform.Find(Constants.RotatorName);
        IsAttacking = false;
    }

    public override void UpdateOverworld()
    {
        if (Input.GetButtonDown(Constants.AttackInput) && _isAttackReady && !_movementController.IsAutoMoving)
        {
            var weapon = TheParty.GetLeader().Weapon;
            Attack(weapon);
        }
    }

    public virtual void Attack(GameObject weaponPrefab)
    {
        if(weaponPrefab)
        {
            var weaponObject = (GameObject)Instantiate(weaponPrefab, 
                transform.position + weaponPrefab.transform.position, 
                _rotator.rotation * weaponPrefab.transform.rotation);
            weaponObject.transform.parent = _rotator;
            _weapon = weaponObject.GetComponent<Weapon>();
            StartCoroutine(AttackCooldown());
        }

        StartCoroutine(AnimateAttack());
    }

    private IEnumerator AnimateAttack()
    {
        IsAttacking = true;
        var animationName = string.Format("{0}{1}", Constants.Attack, _movementController.FacingDirection);
        var animationLength = Utils.GetAnimationClipLength(_animator, animationName);
        _animator.Play(animationName);
        yield return new WaitForSeconds(animationLength + .1f);
        IsAttacking = false;
    }

    private IEnumerator AttackCooldown()
    {
        _isAttackReady = false;
        yield return new WaitForSeconds(_weapon.GetCooldown());
        _isAttackReady = true;
    }


    #region Animation Events
    public void UpAttack1() { if(_weapon) _weapon.Up1(); }
    public void UpAttack2() { if (_weapon) _weapon.Up2(); }
    public void UpAttack3() { if (_weapon) _weapon.Up3(); }
    public void UpAttack4() { if (_weapon) _weapon.Up4(); }

    public void DownAttack1() { if (_weapon) _weapon.Down1(); }
    public void DownAttack2() { if (_weapon) _weapon.Down2(); }
    public void DownAttack3() { if (_weapon) _weapon.Down3(); }
    public void DownAttack4() { if (_weapon) _weapon.Down4(); }

    public void LeftAttack1() { if (_weapon) _weapon.Left1(); }
    public void LeftAttack2() { if (_weapon) _weapon.Left2(); }
    public void LeftAttack3() { if (_weapon) _weapon.Left3(); }
    public void LeftAttack4() { if (_weapon) _weapon.Left4(); }

    public void RightAttack1() { if (_weapon) _weapon.Right1(); }
    public void RightAttack2() { if (_weapon) _weapon.Right2(); }
    public void RightAttack3() { if (_weapon) _weapon.Right3(); }
    public void RightAttack4() { if (_weapon) _weapon.Right4(); }

    public void EndAttack() { if (_weapon) _weapon.End(); }
    #endregion
}
