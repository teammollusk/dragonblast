﻿using UnityEngine;
using System.Collections;

public class Player : Character {

    private GameObject _partyMemberPrefab = null;
    private bool _inPartyMemberDialogue = false;

    public Vector3 LastCheckpoint { get; set; }
    public bool IsAutoMoving { get { return MovementController.IsAutoMoving; } }

    public void Start()
    {
        LastCheckpoint = transform.position;
    }

    public override void UpdateOverworld()
    {
        if(_inPartyMemberDialogue)
        {
			TheGame.TriggerState(GameState.Dialogue);
        }
    }

    public void EnableMovement()
    {
        MovementController.Pause(false);
    }

    public void DisableMovement()
    {
        MovementController.Pause(true);
    }

    public void AutoMoveTo(Vector3 destination, string finalFacingDirection)
    {
        MovementController.AutoMoveTo(destination, finalFacingDirection);
    }

    /// <summary>
    /// Since the main camera is attached to player, we just pan the Player while it's hidden during battle construction/destruction
    /// </summary>
    /// <returns></returns>
    public IEnumerator PanToPosition(Vector3 toPosition)
    {
        const float panSpeed = 6f;
        const float timeInterval = .01f;
        while (true)
        {
            if (transform.position == toPosition)
            {
                yield break;
            }

            transform.position = Vector3.MoveTowards(transform.position, toPosition, Time.deltaTime * panSpeed);

            yield return new WaitForSeconds(timeInterval);
        }
    }

    public void Respawn()
    {
        transform.position = LastCheckpoint;
        Show();
    }

    public void StartPartyMemberDialogue(PartyMemberName partyMemberName, string dialogue, IDialogueEvent dialogueEvent = null)
    {
		TheGame.TriggerState(GameState.Dialogue);
		StartCoroutine(PartyMemberDialogueRoutine(partyMemberName, dialogue, dialogueEvent));
    }

    private IEnumerator PartyMemberDialogueRoutine(PartyMemberName partyMemberName, string dialogue, IDialogueEvent dialogueEvent = null )
    {
        _inPartyMemberDialogue = true;

        var partyMemberDestination = Utils.FindFreeSpaceNearMe(transform.position, MovementController.FacingDirection);

        // Get the party member and make him walk to his destination
        var partyMember = TheParty.GetMember(partyMemberName);
		var partyMemberGameObject = TheObjectPool.Get(PooledObjects.PartyMember);
		partyMemberGameObject.transform.SetPositionAndRotation(transform.position, Quaternion.identity);
		partyMemberGameObject.SetActive(true);
        partyMemberGameObject.GetComponent<Animator>().runtimeAnimatorController = partyMember.AnimatorController;
        var partyMemberMovement = partyMemberGameObject.GetComponent<MovementController>();
        partyMemberMovement.AutoMoveTo(partyMemberDestination);
        yield return new WaitUntil(() => !partyMemberMovement.IsAutoMoving);

        // Face the party leader and the party member towards each other
        MovementController.Face(Utils.GetFacingDirection(transform.position, partyMemberDestination));
        partyMemberMovement.Face(Utils.GetFacingDirection(partyMemberDestination, transform.position));

        // Commence dialogue
        var dialogueObject = SourcelessDialogue.Create(dialogue, dialogueEvent);
        yield return new WaitUntil(() => dialogueObject == null || !dialogueObject.activeSelf);

        // Make party member return inside party leader
        partyMemberMovement.AutoMoveTo(transform.position);
        yield return new WaitUntil(() => !partyMemberMovement.IsAutoMoving);
		partyMemberGameObject.SetActive(false);

        _inPartyMemberDialogue = false;

		TheGame.TriggerState(GameState.Overworld);
	}
}
