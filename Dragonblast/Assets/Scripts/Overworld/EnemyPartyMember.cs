using UnityEngine;
using System.Collections;

[System.Serializable]
public class EnemyPartyMember : PartyMember
{
    public EnemyPartyMember(EnemyPartyMember member, int startingPosition)
    {
        Name = member.Name;
        Health = member.Health;
        Damage = member.Damage;
        Speed = member.Speed;
        StartingPosition = startingPosition;
        TimelineIcon = member.TimelineIcon;
        AnimatorController = member.AnimatorController;
        Weapon = member.Weapon;
        Skills = member.Skills;
        AttackDistance = member.AttackDistance;
    }
}
