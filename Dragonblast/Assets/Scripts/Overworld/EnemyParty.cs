﻿using UnityEngine;
using System.Collections;

public class EnemyParty : MonoBehaviour {

    [SerializeField]
    private EnemyPartyMemberComponent[] _enemyPrefabs = default;
    private EnemyPartyMember[] _partyMembers;

	public EnemyPartyMember[] PartyMembers
    {
        get
        {
            if(_partyMembers == null)
            {
                _partyMembers = new EnemyPartyMember[_enemyPrefabs.Length];
                for(int i = 0; i < _enemyPrefabs.Length; i++)
                {
                    var startingPosition = i * 3; // Offset starting positions 3 away from each other to make enemies with same speed visually distinguishable on the timeline
                    var enemyPartyMember = new EnemyPartyMember(_enemyPrefabs[i].Member, startingPosition);
                    _partyMembers[i] = enemyPartyMember;
                }
            }

            return _partyMembers;
        }
    }
}
