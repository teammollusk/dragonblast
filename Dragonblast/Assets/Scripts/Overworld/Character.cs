﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Character : GameFSMBehaviour {

    private SpriteRenderer[] _sprites;
    private Collider2D[] _colliders;

    protected MovementController MovementController { get; set; }

    public void Awake()
    {
        _sprites = GetComponentsInChildren<SpriteRenderer>();
        _colliders = GetComponentsInChildren<Collider2D>();
        MovementController = GetComponent<MovementController>();
    }

    public void Hide()
    {
        HideSprites();
        DisableColliders();
    }

    public void HideSprites()
    {
        foreach (var sprite in _sprites)
        {
            sprite.enabled = false;
        }
    }

    public void DisableColliders()
    {
        foreach (var collider in _colliders)
        {
            collider.enabled = false;
        }
    }

    public void Show()
    {
        ShowSprites();
        EnableColliders();
    }

    public void ShowSprites()
    {
        foreach (var sprite in _sprites)
        {
            sprite.enabled = true;
        }
    }

    public void EnableColliders()
    {
        foreach (var collider in _colliders)
        {
            collider.enabled = true;
        }
    }

    public void DimSprites()
    {
        foreach(var sprite in _sprites)
        {
            Color dimmedColor = new Color(.6f, .6f, .6f, .5f); // semitransparent white

            sprite.color = dimmedColor;
        }
    }

    public void UnDimSprites()
    {
        foreach(var sprite in _sprites)
        {
            sprite.color = Color.white;
        }
    }

    public void Face(string direction)
    {
        MovementController.Face(direction);
    }

    public bool IsVisible()
    {
        return _sprites[0].isVisible;
    }
}
