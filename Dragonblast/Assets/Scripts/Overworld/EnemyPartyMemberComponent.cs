﻿using UnityEngine;
using System.Collections;

public class EnemyPartyMemberComponent : MonoBehaviour {

    [SerializeField]
    private EnemyPartyMember _member;

    public EnemyPartyMember Member { get { return _member; } set { _member = value; } }

}
