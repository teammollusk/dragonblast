﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PlayStickball : GameFSMBehaviour, IDialogueEvent
{
    [SerializeField]
    private GameObject _ball = default;
    [SerializeField]
    private Gate _gate = default;
    [SerializeField]
    private Player _player = default;

    private Rigidbody2D _playerRigidbody;
    private Dialogue _scottDialogue;
    private MovementController _scottMovement;
    private Attacker _scottAttacker;
    private Animator _animator;
    private const float ballThrowDelay = 2f;
    private float initialBallThrowDelay = 2f;
    private int _weaponHits = 0;
    private const int _weaponHitsNeeded = 3;
    private bool _startedThrowingBalls = false;
    private bool _resumeThrowingBalls = false;
    private const string _endDialogue = "Scott|Alright well that was more fun than cooking a spaghetti dinner, but I think I'm good on stickball. Wanna go get burritos?;Justin|Yeah let's do it.";
    private int _successDialogueIndex = 0;

    private List<string> _successDialogues = new List<string>()
    {
        "Scott|BOOM BABY!",
        "Scott|SLAM!",
        "Scott|NICE!",
        "Scott|THAT WENT SO FAR!",
        "Scott|SPOT ON!",
        "Scott|WOOOOOOOO!",
        "Scott|GOOD JOB!"
    };

    private int _failDialogueIndex = 0;
    private List<string> _failDialogues = new List<string>()
    {
        "Scott|Dang...",
        "Scott|Yikes...",
        "Scott|That sucked.",
        "Scott|Were you even trying to hit the ball?",
        "Scott|That was not smart."
    };


    private readonly Vector3 _playerDestination = new Vector3(-5f, 0.6f, 0);

    public void Start()
    {
        _scottDialogue = GetComponent<Dialogue>();
        _scottMovement = GetComponent<MovementController>();
        _scottAttacker = GetComponent<Attacker>();
        _playerRigidbody = _player.GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
    }

    public override void UpdateDialogue()
    {
        StopAllCoroutines();
        if(_startedThrowingBalls)
            _resumeThrowingBalls = true;
    }

    public override void UpdateOverworld()
    {
        if(_resumeThrowingBalls)
        {
            StartCoroutine("ThrowBalls");
            _resumeThrowingBalls = false;
        }
    }


    public void Execute()
    {
        if (_scottDialogue.DialogueText.Contains("Go stand over there."))
        {
            _scottDialogue.enabled = false;
            _playerRigidbody.isKinematic = true; // The player moves when he's hit by the ball otherwise.
            _scottMovement.Face(Constants.Left);
            StartCoroutine(MoveJustinIntoPosition());
            if(!_startedThrowingBalls)
                StartCoroutine("ThrowBalls");
        }
    }

    public void RegisterWeaponHit()
    {
        _weaponHits++;
        var dialogue = (_weaponHits == _weaponHitsNeeded) ? _endDialogue : _successDialogues[_successDialogueIndex];
        _successDialogueIndex++;
        if(_successDialogueIndex == _successDialogues.Count)
        {
            _successDialogueIndex = 0;
            Utils.ShuffleList(_successDialogues);
        }
        SourcelessDialogue.Create(dialogue);
    }

    public void RegisterPlayerHit()
    {
        _weaponHits = 0;
        var dialogue = _failDialogues[_failDialogueIndex];
        _failDialogueIndex++;
        if(_failDialogueIndex == _failDialogues.Count)
        {
            _failDialogueIndex = 0;
            Utils.ShuffleList(_failDialogues);
        }
        SourcelessDialogue.Create(dialogue);
    }

    private IEnumerator MoveJustinIntoPosition()
    {
        _player.AutoMoveTo(_playerDestination, Constants.Right);
        yield return new WaitUntil(() => !_player.IsAutoMoving);
        _player.DisableMovement();
    }

    private IEnumerator ThrowBalls()
    {
        _startedThrowingBalls = true;
        yield return new WaitForSeconds(initialBallThrowDelay);
        initialBallThrowDelay = 0; // The initial wait is for Justin to walk into position. Subsequent runs of this coroutine should therefor not have it.
        while (_weaponHits < _weaponHitsNeeded)
        {
            _scottAttacker.Attack(null);
            var attackAnimation = Constants.Attack + Constants.Left;
            var animationDuration = Utils.GetAnimationClipLength(_animator, attackAnimation);
            yield return new WaitForSeconds(animationDuration * .65f); // throw ball near end of attack animation
            var ball = Instantiate(_ball);
            ball.transform.parent = transform;
            yield return new WaitForSeconds(ballThrowDelay);
        }
        _scottDialogue.Disable();
        _scottMovement.AutoMoveTo(_player.transform.position);
        while(_scottMovement.IsAutoMoving)
        {
            yield return new WaitForEndOfFrame();
        }
        Destroy(_scottMovement.gameObject);
        _playerRigidbody.isKinematic = false; // Revert isKinematic. See Execute().
        _player.EnableMovement();
        _gate.Open();
        SourcelessDialogue.Create("Scott joined the party!;Scott|I'm inside you.");
        this.enabled = false;
    }
}
