﻿using UnityEngine;
using System.Collections;
using System;

public class AggroBehaviour_MoveFast : MonoBehaviour, IAggroBehaviour
{
    private MovementController _movementController;

    public void Start()
    {
        _movementController = GetComponent<MovementController>();
    }

    public void Execute()
    {
        _movementController.Speed = Constants.EnemyMovementSpeedAggroed;

    }

    public void Undo()
    {
        _movementController.Speed = Constants.EnemyMovementSpeed;
    }
}