﻿using UnityEngine;
using System.Collections;

public interface IAggroBehaviour
{
    void Execute();
    void Undo();
}