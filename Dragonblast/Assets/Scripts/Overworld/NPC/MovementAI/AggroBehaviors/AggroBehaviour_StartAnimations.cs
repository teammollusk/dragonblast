﻿using UnityEngine;
using System.Collections;
using System;

public class AggroBehaviour_StartAnimations : MonoBehaviour, IAggroBehaviour
{
    private Animator _animator;

    public void Start()
    {
        _animator = GetComponent<Animator>();
    }

    public void Execute()
    {
        _animator.enabled = true;
    }

    public void Undo()
    {
        _animator.enabled = false;
    }
}