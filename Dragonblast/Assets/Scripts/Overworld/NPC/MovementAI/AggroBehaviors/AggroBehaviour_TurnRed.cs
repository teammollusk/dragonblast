﻿using UnityEngine;
using System.Collections;
using System;

public class AggroBehaviour_TurnRed : MonoBehaviour, IAggroBehaviour
{
    private SpriteRenderer _spriteRenderer;

    public void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void Execute()
    {
        _spriteRenderer.color = Color.red;
    }

    public void Undo()
    {
        _spriteRenderer.color = Color.white;
    }
}