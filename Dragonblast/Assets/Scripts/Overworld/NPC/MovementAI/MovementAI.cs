﻿using UnityEngine;
using System.Collections;

public abstract class MovementAI : MonoBehaviour
{
    public abstract Vector2 ChooseDirection();
}
