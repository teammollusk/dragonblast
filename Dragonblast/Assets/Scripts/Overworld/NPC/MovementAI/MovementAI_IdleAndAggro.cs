﻿using UnityEngine;
using System.Collections;

public class MovementAI_IdleAndAggro : MovementAI
{
    [SerializeField]
    private bool _idleMovement = true;

    private bool _movementSleeping = false;
    private bool _hasTarget = false;
    private Vector2 _startingPosition;
    private Vector2 _currentDirection;
	private GameObject _player;
    private IAggroBehaviour[] _aggroBehaviours;

    private const float TimeBetweenDirChange = 1;
    private const float TimeBetweenDirChangeChasing = 0;
    private const float PercentChanceToStandStill = .5f;


    public void Start()
    {
        _startingPosition = new Vector2(transform.position.x, transform.position.y);
        _aggroBehaviours = GetComponents<IAggroBehaviour>();
    }

	public void OnTriggerEnter2D(Collider2D other) 
	{
		if(other.tag == Constants.PlayerTag)
		{
			_hasTarget = true;
			_player = other.gameObject;

            foreach(var behaviour in _aggroBehaviours)
            {
                behaviour.Execute();
            }

            StopAllCoroutines();
            _movementSleeping = false;
		}
	}

	public void OnTriggerExit2D(Collider2D other) 
	{
		if(other.tag == Constants.PlayerTag)
		{
			_hasTarget = false;

            foreach (var behaviour in _aggroBehaviours)
            {
                behaviour.Undo();
            }

            StopAllCoroutines();
            _movementSleeping = false;
        }
	}

    public override Vector2 ChooseDirection()
    {
        if (_hasTarget && !_movementSleeping)
        {
            _currentDirection = _player.transform.position - transform.position;
            StartCoroutine(SleepMovement(TimeBetweenDirChangeChasing));
        }
        else if (!_movementSleeping)
        {
            if (_idleMovement)
            {
                _currentDirection = GetRandomTetheredDirection();
            }
            else
            {
                _currentDirection = Vector2.zero;
            }
            StartCoroutine(SleepMovement(TimeBetweenDirChange));
        }
        return _currentDirection;
    }

	public void Reset()
	{
		_hasTarget = false;
	}

	private Vector2 GetRandomTetheredDirection()
    {
        Vector2 dir;
        float xMin = -1f;
        float xMax = 1f;
        float yMin = -1f;
        float yMax = 1f;

        // If you're to the right of your starting point, don't go further right, etc
        if (transform.position.x < _startingPosition.x)
            xMin = 0;
        if (transform.position.x > _startingPosition.x)
            xMax = 0;
        if (transform.position.y < _startingPosition.y)
            yMin = 0;
        if (transform.position.y > _startingPosition.y)
            yMax = 0;

        // Stay still X% of the time, move about the rest
        if (Random.Range(0f, 1f) < PercentChanceToStandStill)
            dir = Vector2.zero;
        else
            dir = new Vector2(Random.Range(xMin, xMax), Random.Range(yMin, yMax));

        return dir;
    }

    private IEnumerator SleepMovement(float time)
    {
        _movementSleeping = true;
        yield return new WaitForSeconds(time);
        _movementSleeping = false;
    }
}
