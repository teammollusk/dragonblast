﻿using UnityEngine;
using System.Collections;

public class Enemy : Character
{
    [SerializeField]
    private CircleCollider2D _targetingCollider = default;

    private Vector3 _startingPosition;
    private Rigidbody2D _rigidbody;
    private CombatInitiation _combatInitiationScript;
    private IEnemyResetter[] _componentsToReset;
    private IAggroBehaviour[] _aggroBehaviours;
    private const float ShowTime = .2f;
    private const float HideTime = .1f;
    private const float BlinkTime = 2f;
    private bool _blinking = false;

    public Vector3 StartingPosition { get { return _startingPosition; } set { _startingPosition = value; } }


    public void Start()
    {
        _startingPosition = transform.position;
        _rigidbody = GetComponent<Rigidbody2D>();
        if(MovementController)
            MovementController.Speed = Constants.EnemyMovementSpeed;
        _combatInitiationScript = GetComponent<CombatInitiation>();
        _componentsToReset = GetComponents<IEnemyResetter>();
        _aggroBehaviours = GetComponents<IAggroBehaviour>();
    }

	public override void OnEnterCombat()
	{
		// Disable colliders and dim all overworld enemies during combat to make it clear who you're fighting
		DimSprites();
		DisableColliders();
	}

	public override void OnExitCombat()
	{
		// Have all enemies Blink right after a combat ends to avoid weird Battle->Overworld->Battle transitions
		// And undo DimSprites from OnEnterCombat()
		UnDimSprites();
		if (IsVisible())
		{
			StartCoroutine(Blink());
		}
		else if (!_blinking)
		{
			Show();

			if ((_combatInitiationScript != null && !_combatInitiationScript.enabled) || (MovementController != null && !MovementController.enabled))
				_targetingCollider.enabled = false;
		}
	}

	void OnBecameInvisible()
    {
        if (!Utils.WorldPointIsVisible(_startingPosition))
        {
            Reset();
        }
    }

    public void StartBlinking()
    {
        StartCoroutine(Blink());
    }

    // Used after running away from a battle to signify the brief invulnerability/statis period
    private IEnumerator Blink()
    {
        _blinking = true;
        MovementController.Pause(true);
        DisableColliders();

        float timeElapsed = 0;
        while (timeElapsed < BlinkTime)
        {
            HideSprites();
            yield return new WaitForSeconds(HideTime);
            ShowSprites();
            yield return new WaitForSeconds(ShowTime);
            
            timeElapsed += (HideTime + ShowTime);
        }

        Show();
        MovementController.Pause(false);
        _blinking = false;
    }

    public void Activate()
    {
        MovementController.enabled = true;
        _combatInitiationScript.enabled = true;
        _rigidbody.isKinematic = false;
        _targetingCollider.enabled = true;
    }

    public void Deactivate()
    {
        MovementController.enabled = false;
        _combatInitiationScript.enabled = false;
        _rigidbody.isKinematic = true;
        _targetingCollider.enabled = false;
    }

    public void Reset()
    {
        transform.position = _startingPosition;
        foreach (var ab in _aggroBehaviours)
        {
            ab.Undo();
        }
        foreach (var ctr in _componentsToReset)
        {
            ctr.Reset();
        }
		// Bleh. Would be great if we could make this more generic.
		MovementAI_IdleAndAggro movementAI = GetComponent<MovementAI_IdleAndAggro>();
		if (movementAI)
			movementAI.Reset();
    }

    public void Die()
    {
        var death = GetComponent<IEnemyDeath>();
        if(death != null)
        {
            death.Execute();
        }
        Destroy(gameObject);
    }
}
