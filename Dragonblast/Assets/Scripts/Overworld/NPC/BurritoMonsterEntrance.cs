﻿using UnityEngine;
using System.Collections;

public class BurritoMonsterEntrance : MonoBehaviour
{
	void Start ()
    {
        StartCoroutine(EntranceRoutine());
	}
	
	private IEnumerator EntranceRoutine()
    {
        const float FallDownSpeed = 10;
        const float FallEndPoint = -49.7f;

        var player = GameObject.FindGameObjectWithTag(Constants.PlayerTag).GetComponent<Player>();
        player.DisableMovement();
        yield return new WaitUntil(() => TheGame.GetState() != GameState.Dialogue);
        SourcelessDialogue.Create("Justin|I... ok then.;Pichotle Employee|Order up!");
        yield return new WaitUntil(() => TheGame.GetState() != GameState.Dialogue);
        var rigidbody = GetComponent<Rigidbody2D>();
        rigidbody.velocity = Vector2.down * FallDownSpeed;
        yield return new WaitUntil(() => transform.position.y < FallEndPoint);
        rigidbody.velocity = Vector2.zero;
        GetComponent<Collider2D>().enabled = true;
        GetComponent<Enemy>().StartingPosition = transform.position;
        yield return new WaitForSeconds(.5f);
        SourcelessDialogue.Create("Justin|Uh oh.");
        player.EnableMovement();
    }
}
