﻿using UnityEngine;
using System.Collections;
using System;

public class EnemyResetter_FirstEnemy : MonoBehaviour, IEnemyResetter
{
    [SerializeField]
    private Collider2D _enemyActivationZone = default;

    private Enemy _enemy;
    private CombatInitiationFirstGnome _combatInit;


    public void Start()
    {
        _enemy = GetComponent<Enemy>();
        _combatInit = GetComponent<CombatInitiationFirstGnome>();
    }

    public void Reset()
    {
        _enemy.Deactivate();
        _combatInit.enabled = true;
        _combatInit.DialogueTriggered = false;
        _enemyActivationZone.enabled = true;
    }
}