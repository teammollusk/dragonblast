﻿using UnityEngine;
using System.Collections;

public interface IEnemyResetter
{
    void Reset();
}