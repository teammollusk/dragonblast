﻿using UnityEngine;
using System.Collections;
using System;

public class EnemyResetter_Garbage : MonoBehaviour, IEnemyResetter
{
    [SerializeField]
    private Sprite _closedGarbageSprite = default;

    private Animator _animator;
    private SpriteRenderer _spriteRenderer;


    public void Start()
    {
        _animator = GetComponent<Animator>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void Reset()
    {
        _animator.enabled = false;
        _spriteRenderer.sprite = _closedGarbageSprite;
    }
}