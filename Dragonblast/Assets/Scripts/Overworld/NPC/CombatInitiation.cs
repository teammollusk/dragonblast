﻿using UnityEngine;
using System.Collections;
using System;

public class CombatInitiation : GameFSMBehaviour
{
    private Player _player;
    private Enemy _thisEnemy;
    protected bool _executed = false;

	// Use this for initialization
	public void Start() 
	{
        _player = GameObject.FindObjectOfType<Player>();
        _thisEnemy = GetComponent<Enemy>();
	}

    public void OnCollisionStay2D(Collision2D collision)
    {
        var initiator = collision.collider.gameObject;
        if (initiator.tag == Constants.PlayerTag)
        {
            Execute(initiator);
        }
    }

    public override void OnEnterOverworld()
    {
        _executed = false;
    }

    public virtual void Execute(GameObject initiator)
    {
        if (!this.enabled || _executed || TheGame.GetState() != GameState.Overworld) return;
        _executed = true;

        if (initiator.tag == Constants.PlayerWeaponTag)
        {
            Debug.Log(string.Format("combat triggered by: {0} (tag={1})", initiator.name, initiator.tag));
            setupCombat(true);
        }
        else if (initiator.tag == Constants.PlayerTag)
        {
            Debug.Log(string.Format("combat triggered by: {0} (tag={1})", initiator.name, initiator.tag));
            var weapon = initiator.GetComponentInChildren<Weapon>();
            if(weapon)
            {
                weapon.Destroy();
            }

            setupCombat(false);
        }
    }

	protected void setupCombat(bool playerWeaponHit)
	{
		TheGame.TriggerState(GameState.Combat);

        // Hide Overworld Player
        _player.Hide();

        // Reset enemy sprite color
        GetComponent<SpriteRenderer>().color = Color.white;

        BattleInitializer.Instance.Execute(this.gameObject, _player.gameObject, playerWeaponHit);

        _thisEnemy.Hide();
    }

}
