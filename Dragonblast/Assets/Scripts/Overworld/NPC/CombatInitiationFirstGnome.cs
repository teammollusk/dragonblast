﻿using UnityEngine;
using System.Collections;
using System;

public class CombatInitiationFirstGnome : CombatInitiation
{
    public bool DialogueTriggered
    {
        get { return _dialogueTriggered; }
        set { _dialogueTriggered = value; }
    }
    private bool _dialogueTriggered = false;
    [SerializeField]
    private BoxCollider2D _enemyActivationZone = default;

    public override void Execute(GameObject initiator)
    {
        if (!this.enabled) return;
        if (initiator.tag == Constants.PlayerWeaponTag)
        {
            Debug.Log(string.Format("combat triggered by: {0} (tag={1})", initiator.name, initiator.tag));
            setupCombat(true);
            _enemyActivationZone.enabled = false;
        }
        else if (initiator.tag == Constants.PlayerTag && _dialogueTriggered == true)
        {
            Debug.Log(string.Format("combat triggered by: {0} (tag={1})", initiator.name, initiator.tag));
            var weapon = initiator.GetComponentInChildren<Weapon>();
            if (weapon)
            {
                weapon.Destroy();
            }

            setupCombat(false);
        }
    }
}
