﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Dervies from attacker but only does the animating, not the weapon generating etc. Used when we want to play an attack animation on an NPC
/// </summary>
public class FakeAttacker : Attacker
{
    public override void UpdateOverworld()
    {
        // Don't perform overworld update, where party leader's weapon is instantiated
    }
}
