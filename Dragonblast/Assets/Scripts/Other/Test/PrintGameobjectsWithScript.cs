﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrintGameobjectsWithScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Period))
        {
            var foundObjects = FindObjectsOfType<MovementAI_IdleAndAggro>();
            for(int i = 0; i < foundObjects.Length; i++)
            {
                Debug.Log(GetGameObjectPath(foundObjects[i].gameObject));
            }
        }
    }

    private string GetGameObjectPath(GameObject obj)
    {
        string path = "/" + obj.name;
        while (obj.transform.parent != null)
        {
            obj = obj.transform.parent.gameObject;
            path = "/" + obj.name + path;
        }
        return path;
    }
}
