﻿using UnityEngine;
using System.Collections;

public class TestPartyMemberDialogue : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D c)
    {
        if(c.tag == Constants.PlayerTag)
        {
            var playerScript = c.GetComponent<Player>();
            playerScript.StartPartyMemberDialogue(PartyMemberName.Scott, "Scott|Yo.");
        }
    }
}