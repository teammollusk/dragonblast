﻿using UnityEngine;
using System.Collections;

public class TestPartyLeaderSwitch : GameFSMBehaviour
{
	public override void UpdateOverworld() 
	{
        if (Input.GetKeyDown("p"))
        {
			CyclePartyLeader();
		}
	}

	public void CyclePartyLeader()
	{
		AllyPartyMember newLeader;
        var justin = TheParty.GetMember("Justin");
        var scott = TheParty.GetMember("Scott");
        var brett = TheParty.GetMember("Brett");


        if (TheParty.GetLeader().Name.Equals("Justin") && scott != null)
		{
            newLeader = scott;
		}
        else if (TheParty.GetLeader().Name.Equals("Scott") && brett != null)
        {
            newLeader = brett;
        }
        else
		{
            newLeader = justin;
		}
		TheParty.SetLeader(newLeader);

		Debug.Log("New leader: " + newLeader.Name);
	}
}